//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Backbase
import ClientCommon

class MockCall<T: Decodable>: Call<T> {
    class MockDataProvider: NSObject, DBSDataProvider {
        func execute(_ request: URLRequest, completionHandler: ((URLResponse?, Data?, Error?) -> Void)? = nil) {}
    }

    let expectedResult: Result<Response<T>, ErrorResponse>

    init(expectedResult: Result<Response<T>, ErrorResponse>) {
        self.expectedResult = expectedResult

        super.init(dataProvider: MockDataProvider(), request: URLRequest(url: URL(fileURLWithPath: "")))
    }

    override func execute(_ completion: @escaping (Result<Response<T>, ErrorResponse>) -> Void) {
        completion(expectedResult)
    }

    override func cancel() -> Bool {
        return true
    }
}
