//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import XCTest
import PocketsClient2
import ClientCommon
import Backbase
import Resolver
import RetailPocketsJourney
@testable import RetailPocketsJourneyPocketsUseCase

private class PocketsExpectations {
    static var expectedPocket = RetailPocketsJourney.Pocket(identifier: "1",
                                                     arrangementId: "arrangementId",
                                                     name: "Pocket Name",
                                                     icon: "icon",
                                                     balance: Currency(amount: "123", currencyCode: "USD"))
    static var expectedList = RetailPocketsJourney.PocketListResponse(pockets: [expectedPocket])
}

private class StubDataProvider: NSObject, DBSDataProvider {
    var called = false

    func execute(_ request: URLRequest, completionHandler: ((URLResponse?, Data?, Error?) -> Void)? = nil) {
        called = true
    }
}

class PocketsUseCaseTests: XCTestCase {
    var client: McokPocketsClient!
    var pocketsUseCase: PocketsServiceUseCase!

    override func setUpWithError() throws {
        try super.setUpWithError()

        try Backbase.initialize("config-stable.json", forceDecryption: false)
        client = McokPocketsClient()
        pocketsUseCase = PocketsUseCase(client: client)
        Resolver.register { self.client as PocketTailorClientAPIProtocol }
    }

    override func tearDown() {
        super.tearDown()

        Resolver.reset()
    }

    func testViewPocketSuccess() {
        let expectation = XCTestExpectation()
        client.setupDefaultSuccess()
        pocketsUseCase.viewPocket(params: .init(pocketId: "1")) { result in
            switch result {
            case .success(let pocket):
                XCTAssertEqual(pocket, PocketsExpectations.expectedPocket)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testCreatePocketSuccess() {
        let expectation = XCTestExpectation()
        client.setupDefaultSuccess()
        pocketsUseCase.createPocket(params: .init(name: "Name", icon: "icon", goal: nil), completion: { result in
            switch result {
            case .success(let pocket):
                XCTAssertEqual(pocket, PocketsExpectations.expectedPocket)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }

    func testListPocketSuccess() {
        let expectation = XCTestExpectation()
        client.setupDefaultSuccess()
        pocketsUseCase.listPockets { result in
            switch result {
            case .success(let searchResult):
                XCTAssertEqual(searchResult, PocketsExpectations.expectedList)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testClosePocketSuccess() {
        let expectation = XCTestExpectation()
        client.setupDefaultSuccess()
        pocketsUseCase.closePocket(params: .init(pocketId: "1"), completion: { result in
            switch result {
            case .success(let response):
                XCTAssertNotNil(response)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }

    func testViewPocketFailure() {
        let expectation = XCTestExpectation()
        client.setupDefaultFailures()
        pocketsUseCase.viewPocket(params: .init(pocketId: "1")) { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 500, data: nil, error: CallError.unsuccessfulHTTPStatusCode(nil)))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testCreatePocketFailure() {
        let expectation = XCTestExpectation()
        client.setupDefaultFailures()
        pocketsUseCase.createPocket(params: .init(name: "Name", icon: "icon", goal: nil), completion: { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 500, data: nil, error: CallError.unsuccessfulHTTPStatusCode(nil)))
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }

    func testListPocketFailure() {
        let expectation = XCTestExpectation()
        client.setupDefaultFailures()
        pocketsUseCase.listPockets { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 500, data: nil, error: CallError.unsuccessfulHTTPStatusCode(nil)))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testClosePocketFailure() {
        let expectation = XCTestExpectation()
        client.setupDefaultFailures()
        pocketsUseCase.closePocket(params: .init(pocketId: "1"), completion: { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 500, data: nil, error: CallError.unsuccessfulHTTPStatusCode(nil)))
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }

    func testViewPocketEmptyResponse() {
        let expectation = XCTestExpectation()
        client.setupEmptyResponse()
        pocketsUseCase.viewPocket(params: .init(pocketId: "1")) { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: CallError.emptyDataResponse))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testCreatePocketEmptyResponse() {
        let expectation = XCTestExpectation()
        client.setupEmptyResponse()
        pocketsUseCase.createPocket(params: .init(name: "Name", icon: "icon", goal: nil), completion: { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: CallError.emptyDataResponse))
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }

    func testListPocketEmptyResponse() {
        let expectation = XCTestExpectation()
        client.setupEmptyResponse()
        pocketsUseCase.listPockets { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: CallError.emptyDataResponse))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testViewPocketCallException() {
        let expectation = XCTestExpectation()
        client.errors = [McokPocketsClient.MockError.generalError]
        pocketsUseCase.viewPocket(params: .init(pocketId: "1")) { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: McokPocketsClient.MockError.generalError))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testCreatePocketCallException() {
        let expectation = XCTestExpectation()
        client.errors = [McokPocketsClient.MockError.generalError]
        pocketsUseCase.createPocket(params: .init(name: "Name", icon: "icon", goal: nil), completion: { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: McokPocketsClient.MockError.generalError))
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }

    func testListPocketCallException() {
        let expectation = XCTestExpectation()
        client.errors = [McokPocketsClient.MockError.generalError]
        pocketsUseCase.listPockets { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: McokPocketsClient.MockError.generalError))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func testClosePocketCallException() {
        let expectation = XCTestExpectation()
        client.errors = [McokPocketsClient.MockError.generalError]
        pocketsUseCase.closePocket(params: .init(pocketId: "1"), completion: { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let errorResponse):
                XCTAssertEqual(errorResponse, RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: McokPocketsClient.MockError.generalError))
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1)
    }
}
