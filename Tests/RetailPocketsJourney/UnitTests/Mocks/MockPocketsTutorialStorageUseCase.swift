//
//  Created by Backbase R&D B.V. on 28/01/2021.
//

import Foundation
@testable import RetailPocketsJourney

// NOTE: encrypted storage cannot be tested therefore we mock user defaults for storage use case.
final class MockPocketsTutorialStorageUseCase: PocketsTutorialStorageUseCase {
    private let tutorialKey = "com.backbase.retail.journey.pockets.isTutorialDone"
    func isFirstTimeJourneyLoading() -> Bool {
        return UserDefaults.standard.string(forKey: tutorialKey) == nil
    }

    func saveTutorialKey() {
        UserDefaults.standard.setValue("true", forKey: tutorialKey)
    }
}
