//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Backbase
import ClientCommon
import PocketsClient2
import RetailPocketsJourney
import Resolver

final public class MockPocketsServiceUseCase: RetailPocketsJourney.PocketsServiceUseCase {
    public init() {}

    public func closePocket(params: ClosePocketRequestParams, completion: @escaping (Result<Void, RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            try client.closePocketCall(closePocketRequest: params.params).execute { result in
                switch result {
                case .success:
                    completion(.success(()))
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    public func viewPocket(params: ViewPocketGetRequestParams, completion: @escaping (Result<RetailPocketsJourney.Pocket, RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            try client.viewPocketCall(pocketId: params.pocketId).execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        completion(.success(Pocket(body)))
                    } else {
                        completion(.failure(.init(statusCode: 0, data: nil, error: CallError.emptyDataResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    public func createPocket(params: CreatePocketPostRequestParams, completion: @escaping (Result<RetailPocketsJourney.Pocket, RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            try client.postPocketCall(pocketPostRequest: params.params).execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        completion(.success(Pocket(body)))
                    } else {
                        completion(.failure(.init(statusCode: 0, data: nil, error: CallError.emptyDataResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    public func listPockets(completion: @escaping (Result<RetailPocketsJourney.PocketListResponse, RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            try client.listPocketsCall().execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        completion(.success(PocketListResponse(body)))
                    } else {
                        completion(.failure(.init(statusCode: 0, data: nil, error: CallError.emptyDataResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    private lazy var client: PocketTailorClientAPIProtocol = {
        if let dbsClient = Backbase.registered(client: PocketTailorClientAPI.self),
           let client = dbsClient as? PocketTailorClientAPI
        {
            return client
        } else if let client = Resolver.optional(PocketTailorClientAPIProtocol.self, name: nil, args: nil) {
            return client
        } else if let client = Resolver.optional(PocketTailorClientAPI.self, name: nil, args: nil) {
            return client
        } else {
            guard let serverURL = URL(string: Backbase.configuration().backbase.serverURL) else {
                fatalError("Invalid or no serverURL found in the SDK configuration.")
            }

            let newServerURL = serverURL
                .appendingPathComponent("api")
                .appendingPathComponent("pocket-tailor")

            let client = PocketTailorClientAPI()
            client.baseURL = newServerURL
            if let dataProvider = Resolver.optional(DBSDataProvider.self) {
                client.dataProvider = dataProvider
                return client
            } else {
                try? Backbase.register(client: client)
                guard let dbsClient = Backbase.registered(client: PocketTailorClientAPI.self),
                      let client = dbsClient as? PocketTailorClientAPI
                else {
                    fatalError("Failed to retrieve Arrangements client")
                }
                return client
            }
        }
    }()
}

extension ClosePocketRequestParams {
    var params: PocketsClient2.ClosePocketRequest {
        PocketsClient2.ClosePocketRequest(id: pocketId)
    }
}

extension RetailPocketsJourney.Pocket {
    init(_ pocket: PocketsClient2.Pocket) {
        self.init(identifier: pocket.id,
                  arrangementId: pocket.arrangementId,
                  name: pocket.name,
                  icon: pocket.icon,
                  goal: PocketGoal(pocket.goal),
                  balance: Currency(pocket.balance))
    }
}

extension RetailPocketsJourney.PocketGoal {
    init?(_ goal: PocketsClient2.PocketGoal?) {
        guard let goal = goal else { return nil }
        let currency: RetailPocketsJourney.Currency?
        if let amountCurrency = goal.amountCurrency {
            currency = RetailPocketsJourney.Currency(amountCurrency)
        } else {
            currency = nil
        }
        self.init(amountCurrency: currency,
                  deadline: goal.deadline,
                  progress: goal.progress)
    }
}

extension RetailPocketsJourney.Currency {
    init(_ currency: PocketsClient2.Currency) {
        self.init(amount: currency.amount,
                  currencyCode: currency.currencyCode,
                  additions: currency.additions)
    }

    var params: PocketsClient2.Currency {
        PocketsClient2.Currency(amount: amount,
                                currencyCode: currencyCode,
                                additions: additions)
    }
}

extension RetailPocketsJourney.CreatePocketPostRequestParams {
    var params: PocketsClient2.PocketPostRequest {
        PocketsClient2.PocketPostRequest(name: name, icon: icon, goal: goal?.params)
    }
}

extension RetailPocketsJourney.PocketListResponse {
    init(_ response: PocketsClient2.PocketListResponse) {
        self.init(pockets: response.pockets.map(Pocket.init))
    }
}

extension RetailPocketsJourney.CreatePocketGoalRequestParams {
    var params: PocketsClient2.PocketGoalRequest {
        PocketsClient2.PocketGoalRequest(amountCurrency: amountCurrency?.params, deadline: deadline)
    }
}

