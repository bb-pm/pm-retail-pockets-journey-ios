//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import ArrangementsClient2
import ClientCommon

class MockArrangementsClient: ProductSummaryAPIProtocol {
    enum MockError: Error {
        case generalError
    }

    var arrangementsCallMocks: [MockCall<[ProductSummaryItem]>] = []
    var errors: [Error] = []

    func getArrangementsByBusinessFunctionCall(businessFunction: String,
                                               resourceName: String,
                                               privilege: String,
                                               contentLanguage: String?,
                                               withLatestBalances: Bool?,
                                               maskIndicator: Bool?,
                                               debitAccount: Bool?,
                                               creditAccount: Bool?,
                                               externalTransferAllowed: Bool?,
                                               productKindName: String?,
                                               legalEntityIds: [String]?,
                                               sourceId: String?,
                                               favorite: Bool?,
                                               searchTerm: String?,
                                               customOrder: Bool?,
                                               favoriteFirst: Bool?,
                                               from: Int?,
                                               size: Int?,
                                               cursor: String?,
                                               orderBy: [OrderByField]?,
                                               direction: SortDirection?) throws -> Call<[ProductSummaryItem]> {
        if !errors.isEmpty {
            throw errors.removeFirst()
        }

        return arrangementsCallMocks.removeFirst()
    }

    func getProductSummaryCall(contentLanguage: String?,
                               debitAccount: Bool?,
                               creditAccount: Bool?,
                               maskIndicator: Bool?) throws -> Call<ProductSummary> {
        throw MockError.generalError
    }

    func getProductSummaryEntitlementsByLegalEntityIdCall(legalEntityIds: [String],
                                                          arrangementIds: [String]?,
                                                          ignoredArrangementIds: [String]?,
                                                          searchTerm: String?,
                                                          from: Int?,
                                                          size: Int?,
                                                          cursor: String?,
                                                          orderBy: [OrderByField]?,
                                                          direction: SortDirection?) throws -> Call<[ProductSummaryItem]> {
        throw MockError.generalError
    }

    func postFilterProductSummariesCall(productSummaryFilterParams: ProductSummaryFilterParams) throws -> Call<ProductSummaryFilterResult> {
        throw MockError.generalError
    }
}
