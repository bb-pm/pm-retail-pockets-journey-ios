//
//  Created by Backbase on 16.04.2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign
import ArrangementsClient2
import ClientCommon

@testable import RetailPocketsJourney

class AccountSelectorViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!
        var client: MockArrangementsClient!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)
            client = MockArrangementsClient()

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
            Resolver.register { config }

            Resolver.register { MockArrangementsServiceUseCase() as ArrangementsServiceUseCase }
            Resolver.register { client as ProductSummaryAPIProtocol }
        }

        afterEach {
            Resolver.reset()
        }

        describe("ViewModel") {

            context("AccountSelector") {

                it("givenViewModel_whenLoadCalled_andLoadingSuccessful_thenItShouldReturnAccounts") {
                    client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: ProductSummaryItem.items)))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount, accountId: "1234", didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: .never())

                    let expectedItems = RetailPocketsJourney.ProductSummaryItem.getExpectedItems(config, {})
                    expect(output.accounts)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(10, expectedItems)]))
                }

                it("givenViewModel_whenAccountSelected_thenItShouldSendTheRightAccountInformation") {
                    client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: ProductSummaryItem.items)))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount, accountId: "1234", didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: .never())

                    var selected: Bool = false
                    let expectedItems = RetailPocketsJourney.ProductSummaryItem.getExpectedItems(config, { selected = true })
                    expect(output.accounts)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(10, expectedItems)]))

                    // select last item
                    expectedItems.last?.selected()
                    XCTAssertTrue(selected)
                }

                it("givenViewModel_whenLoadCalled_andLoadingSuccessful_thenItShouldReturnTheLoadingIndicatorRight") {
                    client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: ProductSummaryItem.items)))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                             accountId: "1234",
                                                             didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: .never())

                    expect(output.isLoading)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, false),
                                   .next(10, true),
                                   .next(10, false)]))
                }

                it("givenViewModel_whenLoadCalled_andLoadingSuccessful_thenItShouldReturnNoEdgeCaseView") {
                    client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: ProductSummaryItem.items)))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                             accountId: "1234",
                                                             didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: .never())

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: Pockets.Error.self, [.next(0, nil), .next(10, nil)]))
                }

                it("givenViewModel_whenLoadCalled_andNoInternet_thenItShouldReturnNoInternetEdgeCase") {
                    let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
                    let errorResponse = ClientCommon.ErrorResponse.error(NSURLErrorNotConnectedToInternet, nil, error)
                    client.arrangementsCallMocks = [.init(expectedResult: .failure(errorResponse))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                             accountId: "1234",
                                                             didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: .never())

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: Pockets.Error.self, [.next(0, nil), .next(10, Pockets.Error.notConnected)]))
                }

                it("givenViewModel_whenLoadCalled_andLoadingFailed_thenItShouldReturnLoadingFailedEdgeCase") {
                    let error = NSError(domain: "", code: 403, userInfo: nil)
                    let errorResponse = ClientCommon.ErrorResponse.error(403, nil, error)
                    client.arrangementsCallMocks = [.init(expectedResult: .failure(errorResponse))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                             accountId: "1234",
                                                             didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: .never())

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: Pockets.Error.self, [.next(0, nil), .next(10, Pockets.Error.loadingFailure(underlying: error))]))
                }

                it("givenViewModel_whenLoadCalled_andLoadingFailed_thenItShouldReturnLoadingFailedEdgeCase_whenRetryClicked_thenItShouldReturnEdgeCaseNil") {
                    let error = NSError(domain: "", code: 403, userInfo: nil)
                    let errorResponse = ClientCommon.ErrorResponse.error(403, nil, error)
                    client.arrangementsCallMocks = [.init(expectedResult: .failure(errorResponse)),
                                                    .init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: ProductSummaryItem.items)))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                             accountId: "1234",
                                                             didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let retry = scheduler.createColdObservable([.next(20, ())])
                    let output = viewModel.bind(load: load.asObservable(), retry: retry.asObservable())

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: Pockets.Error.self, [.next(0, nil), .next(10, Pockets.Error.loadingFailure(underlying: error)), .next(20, nil)]))
                }

                it("givenViewModel_whenRetryCalled_thenItShoulReturnAccountsSuccessfully") {
                    client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: ProductSummaryItem.items)))]

                    let params = AccountSelector.EntryParams(selectionType: .fromAccount,
                                                             accountId: "1234",
                                                             didSelectAccount: { _ in })
                    let viewModel = AccountSelectorViewModel(entryParams: params)

                    let retry = scheduler.createColdObservable([.next(10, ())])
                    let output = viewModel.bind(load: .never(), retry: retry.asObservable())

                    let expectedItems = RetailPocketsJourney.ProductSummaryItem.getExpectedItems(config, {})

                    expect(output.accounts)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(10,  expectedItems)]))
                }
            }
        }
    }
}
