//
//  Created by Backbase R&D B.V. on 30/09/2020.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import PocketsClient2
import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

final class PocketsOverviewViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var client: MockPocketsClient!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)
            client = MockPocketsClient()

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }

            Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                                 presentableDescription: "$")) }
            Resolver.register { MockPocketsServiceUseCase() as PocketsServiceUseCase }
            Resolver.register { client as PocketTailorClientAPIProtocol }
        }

        afterEach {
            Resolver.reset()
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        }

        describe("ViewModel") {

            context("Tutorial") {

                it("givenViewModel_whenLoadTutorialCalled_thenItShouldLoadTutorialScreen") {
                    let viewModel = PocketsOverviewViewModel()
                    var didSelect = false
                    viewModel.didSelectTutorial = {
                        didSelect = true
                    }

                    let loadTutorialAtStart = scheduler.createColdObservable([.next(10, ())])
                    let _ = viewModel.bind(load: .never(),
                                           refresh: .never(),
                                           loadTutorialAtStart: loadTutorialAtStart.asObservable(),
                                           tapRightBar: .never(),
                                           edgeCaseAction: .never(),
                                           pocketSelected: .never())
                    scheduler.advanceTo(20)
                    expect(didSelect).to(equal(true))
                }

                it("givenViewModel_whenLoadTutorialAtStartCalledForSecondTime_thenItShouldNotLoadTutorialScreen") {
                    client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                        header: [:],
                                                                                        body: PocketListResponse(pockets: []))))]

                    Resolver.register { MockPocketsTutorialStorageUseCase() as PocketsTutorialStorageUseCase }
                    let viewModel = PocketsOverviewViewModel()
                    var didSelectArray: [Bool] = [false]
                    viewModel.didSelectTutorial = {
                        didSelectArray.append(true)
                    }

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let loadTutorialAtStart = scheduler.createColdObservable([.next(10, ()), .next(20, ())])
                    let output = viewModel.bind(load: load.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: loadTutorialAtStart.asObservable(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())
                    expect(output.isLoading).events(scheduler: scheduler, disposeBag: disposeBag).to(equal([.next(10, true), .next(10, false)]))
                    scheduler.advanceTo(30)
                    expect(didSelectArray).to(equal([false, true]))
                }

                it("givenViewModel_whenLoadTutorialCalled_thenItShouldLoadTutorialScreen") {
                    client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                        header: [:],
                                                                                        body: PocketListResponse(pockets: []))))]

                    Resolver.register { MockPocketsTutorialStorageUseCase() as PocketsTutorialStorageUseCase }
                    let viewModel = PocketsOverviewViewModel()
                    var didSelectArray: [Bool] = [false]
                    viewModel.didSelectTutorial = {
                        didSelectArray.append(true)
                    }

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let loadTutorial = scheduler.createColdObservable([.next(10, ()), .next(20, ())])
                    let _ = viewModel.bind(load: load.asObservable(),
                                           refresh: .never(),
                                           loadTutorialAtStart: .never(),
                                           tapRightBar: loadTutorial.asObservable(),
                                           edgeCaseAction: .never(),
                                           pocketSelected: .never())
                    scheduler.advanceTo(30)
                    expect(didSelectArray).to(equal([false, true, true]))
                }

                it("givenViewModel_whenLoadTutorialAtStartCalled_whenLoadTutorialCalled_thenItShouldLoadTutorialScreen") {
                    client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                        header: [:],
                                                                                        body: PocketListResponse(pockets: []))))]

                    Resolver.register { MockPocketsTutorialStorageUseCase() as PocketsTutorialStorageUseCase }
                    let viewModel = PocketsOverviewViewModel()
                    var didSelectArray: [Bool] = [false]
                    viewModel.didSelectTutorial = {
                        didSelectArray.append(true)
                    }

                    let load = scheduler.createColdObservable([.next(10, ())])
                    let loadTutorialAtStart = scheduler.createColdObservable([.next(10, ()), .next(20, ())])
                    let loadTutorial = scheduler.createColdObservable([.next(10, ()), .next(20, ())])
                    let _ = viewModel.bind(load: load.asObservable(),
                                           refresh: .never(),
                                           loadTutorialAtStart: loadTutorialAtStart.asObservable(),
                                           tapRightBar: loadTutorial.asObservable(),
                                           edgeCaseAction: .never(),
                                           pocketSelected: .never())
                    scheduler.advanceTo(30)
                    expect(didSelectArray).to(equal([false, true, true, true]))
                }
            }

            context("Loading indicator") {

                it("givenViewModel_whenLoadCalled_thenItShouldReturnTheLoadingProgress") {
                    client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                        header: [:],
                                                                                        body: PocketListResponse(pockets: []))))]
                    let viewModel = PocketsOverviewViewModel()
                    viewModel.didSelectTutorial = { }
                    let load = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(load: load.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())
                    expect(output.isLoading)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(10, true), .next(10, false)]))
                }

                it("givenViewModel_whenRefreshCalled_thenItShouldReturnTheLoadingProgressForRefresh") {
                    client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                        header: [:],
                                                                                        body: PocketListResponse(pockets: []))))]
                    let viewModel = PocketsOverviewViewModel()
                    viewModel.didSelectTutorial = { }
                    let refresh = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(load: .never(),
                                                refresh: refresh.asObservable(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())

                    expect(output.isRefreshing)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(10, true), .next(10, false)]))
                }
            }

            context("Edge cases") {

                it("givenViewModel_whenLoadingCalled_andEdgeCaseHappens_thenItShouldReturnShouldShowRefreshControlFalse") {
                    let nsError = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
                    client.listPocketsCallMocks = [
                        .init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, nsError)))
                    ]
                    let viewModel = PocketsOverviewViewModel()
                    viewModel.didSelectTutorial = { }
                    let load = scheduler.createColdObservable([.next(10, ())])

                    let output = viewModel.bind(load: load.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())

                    expect(output.shouldShowRefreshControl)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                    .to(equal([.next(0, true), .next(10, false)]))
                }

                it("givenViewModel_whenLoadCalled_whenNoPockets_thenItShouldReturnEmptyEdgeCase") {
                    client.listPocketsCallMocks = [.init(expectedResult: .success(.init(statusCode: 200,
                                                                                        header: [:],
                                                                                        body: PocketListResponse(pockets: []))))]
                    let viewModel = PocketsOverviewViewModel()
                    viewModel.didSelectTutorial = { }
                    let load = scheduler.createColdObservable([.next(10, ())])
                    let edgeCaseObserver = scheduler.createObserver(EdgeCase?.self)
                    scheduler.start()

                    let output = viewModel.bind(load: load.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())
                    output.edgeCase.drive(edgeCaseObserver).disposed(by: disposeBag)
                    scheduler.advanceTo(20)
                    let edgeCase = edgeCaseObserver.events.last.map { $0.value.element! }!

                    expect(edgeCase?.title).to(equal("Save more with pockets!"))
                    expect(edgeCase?.subtitle).to(equal("Set up a dedicated space to help you stay\non track with your saving goals."))
                    expect(edgeCase?.icon).notTo(beNil())
                    expect(edgeCase?.actionButtonTitle).to(equal("Create pocket"))
                }

                it("givenViewModel_whenLoadCalled_whenNoPockets_whenEdgeCaseButtonTapped_thenItShouldTriggerNewPocketsCallback") {
                    let currency = PocketsClient2.Currency(amount: "0", currencyCode: "USD")
                    let pocket = Pocket(id: "0", arrangementId: "", name: "", icon: "", balance: currency)
                    client.listPocketsCallMocks = [
                        .init(expectedResult: .success(.init(statusCode: 200, header: [:], body: PocketListResponse(pockets: [])))),
                        .init(expectedResult: .success(.init(statusCode: 200, header: [:], body: PocketListResponse(pockets: [pocket])))),
                    ]

                    let viewModel = PocketsOverviewViewModel()
                    var didSelectArray: [Bool] = [false]
                    viewModel.didSelectCreatePocket = {
                        didSelectArray.append(true)
                    }

                    let action1 = scheduler.createColdObservable([.next(10, ())])
                    let action2 = scheduler.createColdObservable([.next(11, ())])
                    let resultObserver = scheduler.createObserver([PocketsOverviewCellModel].self)
                    scheduler.start()

                    let output = viewModel.bind(load: action1.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: action2.asObservable(),
                                                pocketSelected: .never())
                    output.pockets.drive(resultObserver).disposed(by: disposeBag)
                    scheduler.advanceTo(20)

                    expect(didSelectArray).to(equal([false, true]))
                }

                it("givenViewModel_whenLoadCalled_thenItShouldLoadTwoPockets") {
                    let currency = PocketsClient2.Currency(amount: "0", currencyCode: "USD")
                    let pocket1 = Pocket(id: "0", arrangementId: "", name: "", icon: "", balance: currency)
                    let pocket2 = Pocket(id: "1", arrangementId: "", name: "", icon: "", balance: currency)
                    client.listPocketsCallMocks = [
                        .init(expectedResult: .success(
                                .init(statusCode: 200, header: [:],
                                      body: PocketListResponse(pockets: [pocket1, pocket2]))))]

                    let viewModel = PocketsOverviewViewModel()
                    let action1 = scheduler.createColdObservable([.next(10, ())])
                    let resultObserver = scheduler.createObserver([PocketsOverviewCellModel].self)
                    scheduler.start()

                    let output = viewModel.bind(load: action1.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())
                    output.pockets.drive(resultObserver).disposed(by: disposeBag)
                    scheduler.advanceTo(20)

                    let el = resultObserver.events.last?.value.element
                    expect(el).notTo(beNil())
                    expect(el?.count).to(equal(2))
                }

                it("givenViewModel_whenLoadCalled_whenNoPockets_whenRefreshButtonTapped_thenItShouldLoadTwoPockets") {
                    let currency = PocketsClient2.Currency(amount: "0", currencyCode: "USD")
                    let pocket1 = Pocket(id: "0", arrangementId: "", name: "", icon: "", balance: currency)
                    let pocket2 = Pocket(id: "1", arrangementId: "", name: "", icon: "", balance: currency)
                    client.listPocketsCallMocks = [
                        .init(expectedResult: .success(
                                .init(statusCode: 200, header: [:],
                                      body: PocketListResponse(pockets: [])))),
                        .init(expectedResult: .success(
                                .init(statusCode: 200, header: [:],
                                      body: PocketListResponse(pockets: [pocket1, pocket2])))),
                    ]

                    let viewModel = PocketsOverviewViewModel()
                    let action1 = scheduler.createColdObservable([.next(10, ())])
                    let action2 = scheduler.createColdObservable([.next(20, ())])
                    let resultObserver = scheduler.createObserver([PocketsOverviewCellModel].self)
                    scheduler.start()

                    let output = viewModel.bind(load: action1.asObservable(),
                                                refresh: action2.asObservable(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())
                    output.pockets.drive(resultObserver).disposed(by: disposeBag)

                    scheduler.advanceTo(15)
                    let emptyArr = resultObserver.events.last?.value.element
                    expect(emptyArr).notTo(beNil())
                    expect(emptyArr?.count).to(equal(0))

                    scheduler.advanceTo(25)
                    let twoItemsArr = resultObserver.events.last?.value.element
                    expect(twoItemsArr).notTo(beNil())
                    expect(twoItemsArr?.count).to(equal(2))
                }

                it("givenViewModel_whenNoConnection_whenLoadCalled_whenHasConnection_whenEdgeCaseTryAgainButtonTapped_thenItShouldLoadTwoPockets") {

                    let amountCurrency = PocketsClient2.Currency(amount: "1200", currencyCode: "USD")
                    let dec2030 = Date(timeIntervalSinceReferenceDate: 60 * 60 * 24 * 365 * 30) // Dec 25, 2030
                    let goal = PocketsClient2.PocketGoal(amountCurrency: amountCurrency, deadline: dec2030, progress: 10)
                    let balance = PocketsClient2.Currency(amount: "120", currencyCode: "USD")
                    let pocket1 = Pocket(id: "0", arrangementId: "", name: "Home", icon: "home", goal: goal, balance: balance)
                    let pocket2 = Pocket(id: "1", arrangementId: "", name: "Holidays", icon: "holidays", balance: balance)

                    let nsError = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
                    client.listPocketsCallMocks = [
                        .init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, nsError))),
                        .init(expectedResult: .success(
                                .init(statusCode: 200, header: [:],
                                      body: PocketListResponse(pockets: [pocket1, pocket2])))),
                    ]

                    let viewModel = PocketsOverviewViewModel()
                    let action1 = scheduler.createColdObservable([.next(10, ())])
                    let action2 = scheduler.createColdObservable([.next(20, ())])
                    let resultObserver = scheduler.createObserver([PocketsOverviewCellModel].self)
                    scheduler.start()

                    let output = viewModel.bind(load: action1.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: action2.asObservable(),
                                                pocketSelected: .never())
                    output.pockets.drive(resultObserver).disposed(by: disposeBag)

                    scheduler.advanceTo(15)
                    let emptyArr = resultObserver.events.last?.value.element
                    expect(emptyArr).notTo(beNil())
                    expect(emptyArr?.count).to(equal(0))

                    let edgeCase = try! output.edgeCase.asObservable().take(1).toBlocking().first() as? EdgeCase
                    expect(edgeCase?.title).to(equal("No internet connection"))
                    expect(edgeCase?.subtitle).to(equal("It seems you’re not connected to a network"))
                    expect(edgeCase?.icon).notTo(beNil())
                    expect(edgeCase?.actionButtonTitle).to(equal("Try again"))

                    scheduler.advanceTo(25)
                    let successfulItemsArr = resultObserver.events.last?.value.element
                    expect(successfulItemsArr).notTo(beNil())
                    expect(successfulItemsArr?.count).to(equal(2))

                    let first = successfulItemsArr?.first
                    expect(first?.goalDateTitle).to(equal("Dec 25, 2030"))
                    expect(first?.goalProgressState).to(equal(.inProgress))
                    expect(first?.pocketIconImage).notTo(beNil())
                    expect(first?.pocketNoGoalComment).to(beNil())
                    expect(first?.pocketProgress).to(equal(0.1))
                    expect(first?.pocketTitle).to(equal("Home"))

                    let second = successfulItemsArr?[1]
                    expect(second?.goalDateTitle).to(beNil())
                    expect(second?.goalProgressState).to(equal(.hidden))
                    expect(second?.pocketIconImage).notTo(beNil())
                    expect(second?.pocketNoGoalComment).to(equal("Set an amount and due date to better keep track of your progress"))
                    expect(second?.pocketProgress).to(equal(0))
                    expect(second?.pocketTitle).to(equal("Holidays"))
                }

                it("givenViewModel_whenLoadOnAppearCalled3Times_thenIsLoadingShouldBeCalled3x2Times") {
                    Resolver.register { MockPocketsTutorialStorageUseCase() as PocketsTutorialStorageUseCase }
                    let viewModel = PocketsOverviewViewModel()

                    let appear = scheduler.createColdObservable([.next(10, ()), .next(30, ()), .next(50, ())])
                    let refreshingObserver = scheduler.createObserver(Bool.self)
                    scheduler.start()

                    let output = viewModel.bind(load: appear.asObservable(),
                                                refresh: .never(),
                                                loadTutorialAtStart: .never(),
                                                tapRightBar: .never(),
                                                edgeCaseAction: .never(),
                                                pocketSelected: .never())
                    output.isLoading.drive(refreshingObserver).disposed(by: disposeBag)

                    client.setupDefaultSuccess()
                    scheduler.advanceTo(20)
                    expect(refreshingObserver.events.count).to(equal(2)) // (true)+(false)

                    client.setupDefaultSuccess()
                    scheduler.advanceTo(40)
                    expect(refreshingObserver.events.count).to(equal(4))

                    client.setupDefaultSuccess()
                    scheduler.advanceTo(60)
                    expect(refreshingObserver.events.count).to(equal(6))
                }
            }
        }
    }
}
