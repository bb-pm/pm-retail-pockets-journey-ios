//
//  Created by Backbase on 16.04.2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign
import PaymentOrderClient2
import ClientCommon

@testable import RetailPocketsJourney

class WithdrawMoneyViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!
        var client: MockPaymentsClient!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)
            client = MockPaymentsClient()

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                      presentableDescription: "$"))
            Resolver.register { config }

            Resolver.register { MockPaymentsServiceUseCase() as PaymentsServiceUseCase }
            Resolver.register { client as PaymentOrdersAPIProtocol }
        }

        afterEach {
            Resolver.reset()
        }

        describe("ViewModel") {

            context("WithdrawMoney") {

                it("givenViewModel_whenAmountIsCalledWithData_thenItShouldReturnRightFormattedAmount") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(10, "123")])
                    let input = TransferMoneyViewInput(loadAccountSelector: .never(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: amount.asObservable(),
                                                       transfer: .never())
                    let output = viewModel.bind(input: input)

                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()
                    XCTAssertEqual(model?.noteInfo.title, "Your transfer will be executed immediately")
                    XCTAssertEqual(model?.transferButtonInfo.title, "Confirm Withdrawal")
                    XCTAssertEqual(model?.accountInfo.titleInfo.title, "To")
                    XCTAssertEqual(model?.accountInfo.placeHolderInfo.title, "Select an account to transfer to")
                    XCTAssertEqual(model?.amountInfo.toolbarActionTitle, "Done")
                    XCTAssertEqual(model?.amountInfo.currencyLabelInfo.title, "$")
                    XCTAssertEqual(model?.amountInfo.subtitleInfo?.title, "You have a total of $1,234.00 in this pocket.")
                    XCTAssertEqual(model?.amountInfo.titleInfo.title, "Amount")
                    XCTAssertEqual(model?.amountInfo.allowedDecimalDigitsCount, 2)

                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(0, nil), .next(10, "123.00")]))
                }

                it("givenViewModel_whenLoadAccountSelectorCalled_thenItShouldCallAccountSelectorCallback") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccountSelector = false
                    viewModel.didSelectAccountSelector = { _ in
                        didSelectAccountSelector = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: .never(),
                                                       transfer: .never())
                    let _ = viewModel.bind(input: input)

                    scheduler.advanceTo(30)
                    XCTAssertTrue(didSelectAccountSelector)
                }

                it("givenViewModel_whenTransferCalledWithoutValuesSelected_thenItShouldReturnValidationError") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    let transfer = scheduler.createColdObservable([.next(10, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: .never(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: .never(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: WithdrawMoney.Error.self, [.next(0, nil), .next(10, WithdrawMoney.Error.missingAllFields)]))
                }

                it("givenViewModel_whenDidSelectAccountSelectorTriggered_andAccountSelected_thenItShouldCallAccountSelectorCallbackWithSelectedAccount") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: .never(),
                                                       transfer: .never())
                    let output = viewModel.bind(input: input)
                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)

                    XCTAssertEqual(model?.noteInfo.title, "Your transfer will be executed immediately")

                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))
                }

                it("givenViewModel_whenTransferCalledWithAmountBiggerThanAccountBalance_thenItShouldReturnValidationError") {
                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let startEditing = scheduler.createColdObservable([.next(20, ())])
                    let endEditing: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "12345.67")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: startEditing.asObservable(),
                                                       didEndEditing: endEditing.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)


                    scheduler.advanceTo(30)
                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 120,
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)

                    scheduler.advanceTo(50)
                    scheduler.advanceTo(60)

                    let edgeCase = try! output.edgeCase.asObservable().take(1).toBlocking().first() as? EdgeCase

                    XCTAssertEqual(edgeCase?.title, "Failed to Withdraw Money")
                    XCTAssertEqual(edgeCase?.subtitle, "Your transfer amount cannot be higher than the pocket balance.")
                    XCTAssertEqual(edgeCase?.actionButtonTitle, "OK")
                }

                it("givenViewModel_whenTransferCalled_thenItShouldValidateTheValues") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234567", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    var didSuccessfullyCreated: Bool = false
                    viewModel.didSucceedWithdrawingMoney = { _ in
                        didSuccessfullyCreated = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let startEditing = scheduler.createColdObservable([.next(20, ()), .next(70, ())])
                    let endEditing: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "12345.67")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: startEditing.asObservable(),
                                                       didEndEditing: endEditing.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)
                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)
                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))

                    scheduler.advanceTo(50)
                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(50, "12,345.67")]))

                    scheduler.advanceTo(60)
                    XCTAssertTrue(didSuccessfullyCreated)

                    scheduler.advanceTo(70)

                    expect(model?.output.enteredAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(70, "12345.67")]))
                }

                it("givenViewModel_whenEverytingIsSelected_andTransferCalled_thenItShouldCallSuccessCallback") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    var didSuccessfullyCreated: Bool = false
                    viewModel.didSucceedWithdrawingMoney = { _ in
                        didSuccessfullyCreated = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "123")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: amount.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)

                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)

                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))

                    scheduler.advanceTo(50)
                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(50, "123.00")]))

                    scheduler.advanceTo(60)
                    XCTAssertTrue(didSuccessfullyCreated)
                }

                it("givenViewModel_whenEverytingIsSelected_andTransferCalledWithFullPocketWithdrawal_thenItShouldCallSuccessCallback") {
                    let paymentOrderResponse = PaymentOrderClient2.PaymentOrdersPostResponse(id: "1234", status: .entered)
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: paymentOrderResponse)))]

                    let balance = RetailPocketsJourney.Currency(amount: "12345", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    var didSuccessfullyCreated: Bool = false
                    viewModel.didSucceedWithdrawingMoney = { _ in
                        didSuccessfullyCreated = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "12345")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: amount.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)

                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)

                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))

                    scheduler.advanceTo(50)
                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(50, "12,345.00")]))

                    scheduler.advanceTo(60)
                    XCTAssertTrue(didSuccessfullyCreated)
                }

                it("givenViewModel_whenEverytingIsSelected_andTransferCalled_andSomethingWentWrong_thenItShouldCallLoadingFailedEdgeCase") {
                    let error = NSError(domain: "", code: 403, userInfo: nil)
                    let errorResponse = ClientCommon.ErrorResponse.error(403, nil, error)
                        client.postPaymentOrderCallMocks = [.init(expectedResult: .failure(errorResponse))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    var didSuccessfullyCreated: Bool = false
                    viewModel.didSucceedWithdrawingMoney = { _ in
                        didSuccessfullyCreated = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "123")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: amount.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)
                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)
                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))

                    scheduler.advanceTo(50)
                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(50, "123.00")]))

                    scheduler.advanceTo(60)
                    XCTAssertFalse(didSuccessfullyCreated)

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: WithdrawMoney.Error.self, [.next(60, WithdrawMoney.Error.loadingFailure(underlying: error))]))
                }

                it("givenViewModel_whenEverytingIsSelected_andTransferCalled_andNoInternet_thenItShouldCallNoInternetEdgeCase") {
                    let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
                    let errorResponse = ClientCommon.ErrorResponse.error(NSURLErrorNotConnectedToInternet, nil, error)
                        client.postPaymentOrderCallMocks = [.init(expectedResult: .failure(errorResponse))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    var didSuccessfullyCreated: Bool = false
                    viewModel.didSucceedWithdrawingMoney = { _ in
                        didSuccessfullyCreated = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "123")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: amount.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)
                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)
                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))

                    scheduler.advanceTo(50)
                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(50, "123.00")]))

                    scheduler.advanceTo(60)
                    XCTAssertFalse(didSuccessfullyCreated)

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: WithdrawMoney.Error.self, [.next(60, WithdrawMoney.Error.notConnected)]))
                }

                it("givenViewModel_whenEverytingIsSelected_andTransferCalled_andInvalidResponse_thenItShouldCallSomethingWentWrongEdgeCase") {
                    client.postPaymentOrderCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                                         header: [:],
                                                                                         body: nil)))]

                    let balance = RetailPocketsJourney.Currency(amount: "1234567890", currencyCode: "USD")
                    let pocket = Pocket(identifier: "1234", arrangementId: "1234", name: "Home", icon: "home", balance: balance)
                    let params = WithdrawMoney.EntryParams(pocket: pocket)
                    let viewModel = WithdrawMoneyViewModel(entryParams: params)

                    var didSelectAccount: DidSelectAccount?
                    viewModel.didSelectAccountSelector = { exitParams in
                        didSelectAccount = exitParams.didSelectAccount
                    }

                    var didSuccessfullyCreated: Bool = false
                    viewModel.didSucceedWithdrawingMoney = { _ in
                        didSuccessfullyCreated = true
                    }

                    let loadAccountSelector = scheduler.createColdObservable([.next(20, ())])
                    let amount: TestableObservable<String?> = scheduler.createColdObservable([.next(40, "12345.67")])
                    let transfer = scheduler.createColdObservable([.next(60, ())])
                    let input = TransferMoneyViewInput(loadAccountSelector: loadAccountSelector.asObservable(),
                                                       didStartEditing: .never(),
                                                       didEndEditing: amount.asObservable(),
                                                       transfer: transfer.asObservable())
                    let output = viewModel.bind(input: input)
                    let model = try! output.transferMoneyViewModel.asObservable().take(1).toBlocking().first()

                    scheduler.advanceTo(30)

                    let productSummaryItem = RetailPocketsJourney.ProductSummaryItem(identifier: "someid",
                                                                                     name: "this is the name of the selected product",
                                                                                     availableBalance: 1234,
                                                                                     currency: "USD",
                                                                                     legalEntityIds: [],
                                                                                     productKindName: "Current Account",
                                                                                     debitCards: [])
                    didSelectAccount?(productSummaryItem)
                    let accountInfo = PaymentPartyViewModel.Output(accountTitle: "this is the name of the selected product",
                                                                   accountBalanceTitle: "Available balance",
                                                                   accountBalanceSubtitle: "$1,234.00")
                    expect(model?.accountInfo.output)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(30, accountInfo)]))

                    scheduler.advanceTo(50)
                    let formattedAmount = model?.output.formattedAmount

                    expect(formattedAmount)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal([.next(50, "12,345.67")]))

                    scheduler.advanceTo(60)
                    XCTAssertFalse(didSuccessfullyCreated)

                    expect(output.edgeCase)
                        .events(scheduler: scheduler, disposeBag: disposeBag)
                        .to(equal(as: WithdrawMoney.Error.self, [.next(60, WithdrawMoney.Error.invalidResponse)]))
                }
            }
        }
    }
}
