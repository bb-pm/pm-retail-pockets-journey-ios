//
//  Created by Backbase on 19.04.2021.
//

import Foundation
import RetailPocketsJourney
import ClientCommon

extension RetailPocketsJourney.ErrorResponse {
    init(statusCode: Int? = nil, data: Data? = nil, error: CallError) {
        self.init(statusCode: statusCode, data: data, error: Self.mapCommonClientError(error))
    }

    init(_ errorResponse: ClientCommon.ErrorResponse) {
        switch errorResponse {
        case let .error(statusCode, data, error as ClientCommon.CallError):
            self.init(statusCode: statusCode, data: data, error: Self.mapCommonClientError(error))
        case let .error(statusCode, data, error):
            self.init(statusCode: statusCode, data: data, error: Self.mapNsError(error: error) ?? error)
        default:
            self.init(statusCode: 0, data: nil, error: NSError())
        }
    }

    static func mapCommonClientError(_ error: ClientCommon.CallError?) -> Error? {
        switch error {
        case .emptyDataResponse:
            return Pockets.Error.invalidResponse
        case .nilHTTPResponse(let detailError), .unsuccessfulHTTPStatusCode(let detailError):
            return Self.mapNsError(error: detailError)
        default:
            return Pockets.Error.loadingFailure(underlying: nil)
        }
    }

    static func mapNsError(error: Swift.Error?) -> Pockets.Error? {
        guard let nsError = (error as NSError?),
              nsError.code == NSURLErrorNotConnectedToInternet ||
                nsError.domain == kCFErrorDomainCFNetwork as String
        else {
            return Pockets.Error.loadingFailure(underlying: error)
        }
        return Pockets.Error.notConnected
    }
}
