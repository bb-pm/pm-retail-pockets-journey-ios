//
//  Created by Backbase on 19.04.2021.
//

import Foundation
import ArrangementsClient2
@testable import RetailPocketsJourney

extension ArrangementsClient2.ProductSummaryItem {
    static var items: [Self] {
        var response: [Self] = []
        guard let data = Bundle.data(from: "arrangements") else { return [] }
        do {
            response = try JSONDecoder().decode([ArrangementsClient2.ProductSummaryItem].self, from: data)
        } catch  {
            print("Error while trying to parse to ProductSummaryItem: \(error.localizedDescription)")
        }
        return response
    }
}

extension RetailPocketsJourney.ProductSummaryItem {
    static func getExpectedItems(_ config: Pockets.Configuration,
                                 _ selected: @escaping () -> Void) -> [AccountItemViewModel] {
        let items = ArrangementsClient2.ProductSummaryItem.items.map(RetailPocketsJourney.ProductSummaryItem.init)
        let arrangements = items.filter { item in
            guard let productKindName = item.productKindName else { return true }
            return                 config.accountSelector.productKindNamesFrom.contains(productKindName) && config.currencyInfo.currencyCode == item.currency
        }

        let expectedItems = arrangements.map { (item) -> AccountItemViewModel in
            let title = config.accountSelector.uiDataMapper.listItemTitle(item)
            let subtitle = config.accountSelector.uiDataMapper.listItemSubtitle(item)
            let subtitle2 = config.accountSelector.uiDataMapper.listItemSubtitle2(item)
            let subtitle3 = config.accountSelector.uiDataMapper.listItemSubtitle3(item)
            return AccountItemViewModel(isPreselected: false,
                                        title: title,
                                        subtitle: subtitle,
                                        subtitle2: subtitle2,
                                        subtitle3: subtitle3,
                                        selected: { selected() })
        }
        return expectedItems
    }
}
