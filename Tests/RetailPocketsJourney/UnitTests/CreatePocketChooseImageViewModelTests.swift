//
//  Created by Backbase R&D B.V. on 17/02/2021.
//

import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble
import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

class CreatePocketChooseImageViewModelTests: QuickSpec {
    override func spec() {
        var disposeBag: DisposeBag!
        var scheduler: TestScheduler!
        var config: Pockets.Configuration!

        beforeEach {
            disposeBag = DisposeBag()
            scheduler = TestScheduler(initialClock: 0)

            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }
            let currencyInfo = CurrencyInfo(currencyCode: "USD", presentableDescription: "$")
            config = Pockets.Configuration(currencyInfo: currencyInfo)
            Resolver.register { config }
        }

        afterEach {
            Resolver.reset()
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        }

        describe("ViewModel") {

            context("content") {

                it("givenViewModel_whenBindCalled_thenItShouldReturnAllImageConfigs") {
                    let viewModel = CreatePocketChooseImageViewModel(selectedImageName: "home", didChooseImage: { _ in })
                    let output = viewModel.bind(newSelectedIndex: .never())

                    let configs = try! output.images.asObservable().take(1).toBlocking().first()
                    expect(configs).notTo(beNil())
                    expect(configs?.count).to(equal(12))

                    let selectedIndex = try! output.selectedIndex.asObservable().take(1).toBlocking().first()
                    expect(selectedIndex).notTo(beNil())
                    expect(selectedIndex).to(equal(0))
                }

                it("givenViewModel_whenBindCalledWithUtilitiesSelected_thenItShouldReturnSelectedIndexForUtilities") {
                    let viewModel = CreatePocketChooseImageViewModel(selectedImageName: "utilities", didChooseImage: { _ in })
                    let output = viewModel.bind(newSelectedIndex: .never())

                    let selectedIndex = try! output.selectedIndex.asObservable().take(1).toBlocking().first()
                    expect(selectedIndex).notTo(beNil())
                    expect(selectedIndex).to(equal(5))
                }

                it("givenViewModel_whenBindCalledWithUtilitiesSelected_andNewPocketImageIsSelected_thenItShouldReturnSelectedIndexForUtilities_andNewPocketImage") {
                    var newSelectedName: String?
                    let viewModel = CreatePocketChooseImageViewModel(selectedImageName: "utilities",
                                                                     didChooseImage: { item in
                                                                        newSelectedName = item.name
                                                                     })

                    let newImageTap = scheduler.createColdObservable([.next(10, 8)])

                    let _ = viewModel.bind(newSelectedIndex: newImageTap.asObservable())

                    scheduler.advanceTo(20)

                    expect(newSelectedName).to(equal("New Phone"))
                }
            }
        }
    }
}
