import RxTest
import RxSwift
import RxCocoa
import Resolver
import RxNimble
import Quick
import Nimble

import Backbase
import RetailJourneyCommon
import RetailDesign

@testable import RetailPocketsJourney

final class PocketsTutorialViewModelTests: QuickSpec {
    override func spec() {
        beforeEach {
            do {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            } catch {
                XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
            }

            Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                                 presentableDescription: "$")) }
        }

        afterEach {
            Resolver.reset()
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        }

        describe("ViewModel") {

            context("content") {

                it("givenViewModel_whenBindCalled_thenItShouldReturnViewContent") {
                    let viewModel = PocketsTutorialViewModel()
                    let output = viewModel.bind()

                    let title = try! output.title.asObservable().take(1).toBlocking().first()
                    expect(title).to(equal("Get started with\nPockets!"))

                    let subtitle = try! output.subtitle.asObservable().take(1).toBlocking().first()
                    expect(subtitle).to(equal("A personalized space for your money that helps you reach your saving goals even faster."))

                    let continueButtonTitle = try! output.continueButtonTitle.asObservable().take(1).toBlocking().first()
                    expect(continueButtonTitle).to(equal("Continue"))

                    let closeButtonIcon = try! output.closeButtonIcon.asObservable().take(1).toBlocking().first()
                    expect(closeButtonIcon).notTo(beNil())

                    let sections = try! output.sections.asObservable().take(1).toBlocking().first()
                    expect(sections?.first?.title).to(equal("Make it yours"))
                    expect(sections?.first?.subtitle).to(equal("Give your pocket its own name and image"))
                    expect(sections?.first?.icon).notTo(beNil())

                    expect(sections?[1].title).to(equal("What's your goal?"))
                    expect(sections?[1].subtitle).to(equal("Set a goal amount and goal date"))
                    expect(sections?[1].icon).notTo(beNil())

                    expect(sections?[2].title).to(equal("Transfer money"))
                    expect(sections?[2].subtitle).to(equal("Easily move money to and from your pocket"))
                    expect(sections?[2].icon).notTo(beNil())
                }
            }
        }
    }
}

