//
//  Created by Backbase R&D B.V. on 30/09/2020.
//

import XCTest
import Backbase
import Resolver

@testable import RetailPocketsJourney

final class PocketsUITests: XCTestCase {
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    private func setLaunchArguments(app: XCUIApplication) {
        app.launchArguments += ["-AppleLanguages", "(en-US)"]
        app.launchArguments += ["-AppleLocale", "en_US"]
        app.launchEnvironment["DataProvider"] = "local"
    }

    private func launchApp() -> XCUIApplication {
        let app = XCUIApplication()
        setLaunchArguments(app: app)
        app.launch()
        return app
    }

    // MARK: - Empty Overview and Tutorial tests

    func testEmptyJourney() {
        let app = XCUIApplication()
        setLaunchArguments(app: app)
        app.launchEnvironment["EmptyList"] = "true"
        app.launch()

        // Show tutorial screen on first launch
        let continueButton = app.buttons.matching(identifier: "PocketsTutorialViewControllerContinueButton").firstMatch
        let continueButtonExists = continueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(continueButtonExists)

        // Dismiss tutorial screen
        continueButton.tap()
        XCTAssertFalse(continueButton.visible(app))

        // Show tutorial screen on info bar bar button click
        let infoBarButton = app.buttons.matching(identifier: "rightBarButtonItem").firstMatch
        let infoBarButtonExists = infoBarButton.waitForExistence(timeout: 5)
        XCTAssertTrue(infoBarButtonExists)
        infoBarButton.tap()

        // Dismiss tutorial screen
        XCTAssertTrue(continueButtonExists)
        continueButton.tap()
        XCTAssertFalse(continueButton.visible(app))

        // Open create pocket view
        let overviewContinueButton = app.buttons["Create pocket"].firstMatch
        let overviewContinueButtonExists = overviewContinueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(overviewContinueButtonExists)
        overviewContinueButton.tap()

        let createPocketView = app.otherElements.matching(identifier: "CreatePocketImageAndNameViewController").firstMatch
        let createPocketViewExists = createPocketView.waitForExistence(timeout: 5)
        XCTAssertTrue(createPocketViewExists)
    }

    // MARK: Overview and create flow

    func testPocketsJourney() {
        let app = launchApp()

        // Show tutorial screen on first launch
        let continueButton = app.buttons.matching(identifier: "PocketsTutorialViewControllerContinueButton").firstMatch
        let continueButtonExists = continueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(continueButtonExists)

        // Dismiss tutorial screen
        continueButton.tap()
        XCTAssertFalse(continueButton.visible(app))

        // Show create pocket view screen on info bar bar button click
        let infoBarButton = app.buttons.matching(identifier: "rightBarButtonItem").firstMatch
        let infoBarButtonExists = infoBarButton.waitForExistence(timeout: 5)
        XCTAssertTrue(infoBarButtonExists)
        infoBarButton.tap()

        let createPocketView = app.otherElements.matching(identifier: "CreatePocketImageAndNameViewController").firstMatch
        let createPocketViewExists = createPocketView.waitForExistence(timeout: 5)
        XCTAssertTrue(createPocketViewExists)

        // Verify default pocket name
        let createNameTextField = app.textFields.matching(identifier: "CreatePocketImageAndNameViewControllerTextField").firstMatch
        let createNameTextFieldExists = createNameTextField.waitForExistence(timeout: 5)
        XCTAssertTrue(createNameTextFieldExists)
        let defaultValue = "Home"
        XCTAssertEqual(createNameTextField.value as? String, defaultValue)
        // Insert new pocket
        createNameTextField.tap()
        let deleteString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: defaultValue.count)
        createNameTextField.typeText(deleteString)
        createNameTextField.typeText("New pocket")
        // Verify new pocket name
        XCTAssertEqual(createNameTextField.value as? String, "New pocket")
        let keyboardDoneButtonExists = app.keyboards.buttons["done"].firstMatch.waitForExistence(timeout: 5)
        XCTAssertTrue(keyboardDoneButtonExists)
        app.keyboards.buttons["done"].firstMatch.tap()

        // Click image button
        let selectImageButton = app.buttons.matching(identifier: "CreatePocketImageAndNameViewControllerChooseImageButton").firstMatch
        let selectImageButtonExists = selectImageButton.waitForExistence(timeout: 5)
        XCTAssertTrue(selectImageButtonExists)
        selectImageButton.tap()

        // Verify the choose image screen is visible
        let chooseImageView = app.otherElements.matching(identifier: "CreatePocketChooseImageViewController").firstMatch
        let chooseImageViewExists = chooseImageView.waitForExistence(timeout: 5)
        XCTAssertTrue(chooseImageViewExists)

        // Verify on new image selection, the screen should dismiss
        let utilitiesCell = app.cells.matching(identifier: "ChooseImageCollectionViewCell_Utilities").firstMatch
        let utilitiesCellExists = utilitiesCell.waitForExistence(timeout: 5)
        XCTAssertTrue(utilitiesCellExists)
        utilitiesCell.tap()

        XCTAssertTrue(createPocketView.visible(app))

        // Verify continue button click, takes us to the pocket goal screen.
        let nameAndImageViewContinueButton = app.buttons.matching(identifier: "CreatePocketImageAndNameViewControllerContinueButton").firstMatch
        XCTAssertTrue(nameAndImageViewContinueButton.visible(app))
        nameAndImageViewContinueButton.tap()

        let createPocketGoalView = app.otherElements.matching(identifier: "CreatePocketGoalViewController").firstMatch
        let createPocketGoalViewExists = createPocketGoalView.waitForExistence(timeout: 5)
        XCTAssertTrue(createPocketGoalViewExists)

        // Verify goal amount input
        let amountInput = app.textFields.matching(identifier: "CreatePocketGoalViewControllerAmount.textInput.textField").firstMatch
        amountInput.typeText("1234")
        XCTAssertEqual(amountInput.value as? String, "1234")

        let switchView = app.switches.firstMatch
        switchView.tap()

        // Verify date input
        let datePicker = app.datePickers.matching(identifier: "InlineDatePickerViewDatePicker").firstMatch
        let datePickerExists = datePicker.waitForExistence(timeout: 5)
        XCTAssertTrue(datePickerExists)

        let date = Date()
        let todaysCellLabel = datePicker.staticTexts[todaysDay(from: date)].firstMatch
        XCTAssertTrue(todaysCellLabel.visible(app))
        todaysCellLabel.forceTapElement()

        let datePickerButton = app.buttons.matching(identifier: "InlineDatePickerViewDateButton").firstMatch
        XCTAssertEqual(datePickerButton.label, dateString(from: date))

        // Verify setting switch off, disappears the date picker
        switchView.tap()
        XCTAssertFalse(datePicker.visible(app))

        // Verify continue button click goes to the review screen.
        let goalContinueButton = app.buttons.matching(identifier: "CreatePocketGoalViewControllerContinueButton").firstMatch
        goalContinueButton.tap()

        let reviewView = app.otherElements.matching(identifier: "CreatePocketReviewViewController").firstMatch
        let reviewViewExists = reviewView.waitForExistence(timeout: 5)
        XCTAssertTrue(reviewViewExists)

        app.navigationBars.buttons.element(boundBy: 0).tap()

        // Verify skip button click goes to the review screen.
        let goalSkipButton = app.navigationBars.buttons.matching(identifier: "CreatePocketGoalViewControllerSkipButton").firstMatch
        goalSkipButton.tap()

        let reviewViewExistsAfterSkip = reviewView.waitForExistence(timeout: 5)
        XCTAssertTrue(reviewViewExistsAfterSkip)

        // Verify review screen, continue button click goes to the success screen.
        let reviewContinueButton = app.buttons.matching(identifier: "CreatePocketReviewViewControllerContinueButton").firstMatch
        reviewContinueButton.tap()

        let successView = app.otherElements.matching(identifier: "SuccessViewController").firstMatch
        let successViewExists = successView.waitForExistence(timeout: 5)
        XCTAssertTrue(successViewExists)

        // Verify success screen, continue button click goes to the pockets overview screen.
        let successContinueButton = app.buttons.matching(identifier: "SuccessViewControllerContinueButton").firstMatch
        successContinueButton.tap()

        let detailsView = app.otherElements.matching(identifier: "PocketDetailsViewController").firstMatch
        let detailsViewExists = detailsView.waitForExistence(timeout: 5)
        XCTAssertTrue(detailsViewExists)
    }

    // MARK: - Add and withdraw money flow

    func testAddMoney() {
        let app = launchApp()

        // Show tutorial screen on first launch
        let continueButton = app.buttons.matching(identifier: "PocketsTutorialViewControllerContinueButton").firstMatch
        let continueButtonExists = continueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(continueButtonExists)

        // Dismiss tutorial screen
        continueButton.tap()
        XCTAssertFalse(continueButton.visible(app))

        let firstCell = app.cells.matching(identifier: "PocketsOverviewCell_0").firstMatch
        let firstCellExists = firstCell.waitForExistence(timeout: 5)
        XCTAssertTrue(firstCellExists)

        // Show pocket details screen.
        firstCell.tap()

        let detailsView = app.otherElements.matching(identifier: "PocketDetailsViewController").firstMatch
        let detailsViewExists = detailsView.waitForExistence(timeout: 5)
        XCTAssertTrue(detailsViewExists)

        let addMoneyButton = app.staticTexts.matching(identifier: "Add money").firstMatch
        let addMoneyButtonExists = addMoneyButton.waitForExistence(timeout: 5)
        XCTAssertTrue(addMoneyButtonExists)

        // Show add money screen
        addMoneyButton.forceTapElement()

        let addMoneyView = app.otherElements.matching(identifier: "AddMoneyViewController").firstMatch
        let addMoneyViewExists = addMoneyView.waitForExistence(timeout: 5)
        XCTAssertTrue(addMoneyViewExists)

        let addMoneyContinueButton = app.buttons.matching(identifier: "TransferMoneyContinueButton").firstMatch
        let addMoneyContinueButtonExists = addMoneyContinueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(addMoneyContinueButtonExists)

        // Select account field to show account selector
        let accountSelectorButton = app.buttons.matching(identifier: "paymentPartyViewButtonID").firstMatch
        let accountSelectorButtonExists = accountSelectorButton.waitForExistence(timeout: 5)
        XCTAssertTrue(accountSelectorButtonExists)

        // Show account selector screen
        accountSelectorButton.tap()

        let accountSelectorView = app.otherElements.matching(identifier: "AccountSelectorViewController").firstMatch
        let accountSelectorViewExits = accountSelectorView.waitForExistence(timeout: 5)
        XCTAssertTrue(accountSelectorViewExits)

        let firstAccount = app.otherElements.matching(identifier: "AccountItemView_0").firstMatch
        let firstAccountExits = firstAccount.waitForExistence(timeout: 5)
        XCTAssertTrue(firstAccountExits)

        // Select account to dismiss page
        firstAccount.tap()
        XCTAssertFalse(accountSelectorView.visible(app))

        // Verify right account is selected
        let accountName = app.staticTexts.matching(identifier: "Our joined account").firstMatch
        let accountNameExists = accountName.waitForExistence(timeout: 5)
        XCTAssertTrue(accountNameExists)

        // Enter amount
        let amountInput = app.textFields.matching(identifier: "AmountInputViewAmountInput.textInput.textField").firstMatch
        amountInput.forceTapElement()
        amountInput.typeText("1234.5.67")
        XCTAssertEqual(amountInput.value as? String, "1234.56")

        let doneButton = app.toolbars.buttons.matching(identifier: "Done").firstMatch
        doneButton.tap()

        // Click continue button and dismiss add money screen
        addMoneyContinueButton.tap()
        
        // Verify add money success screen presented
        let successView = app.otherElements.matching(identifier: "SuccessViewController").firstMatch
        let successViewExists = successView.waitForExistence(timeout: 5)
        XCTAssertTrue(successViewExists)
        
        // Click continue button to open pocket details
        let successContinueButton = app.buttons.matching(identifier: "SuccessViewControllerContinueButton").firstMatch
        successContinueButton.tap()

        // Verify pocket details opened
        let detailsViewExists2 = detailsView.waitForExistence(timeout: 5)
        XCTAssertTrue(detailsViewExists2)
    }

    func testWithdrawMoney() {
        let app = launchApp()

        // Show tutorial screen on first launch
        let continueButton = app.buttons.matching(identifier: "PocketsTutorialViewControllerContinueButton").firstMatch
        let continueButtonExists = continueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(continueButtonExists)

        // Dismiss tutorial screen
        continueButton.tap()
        XCTAssertFalse(continueButton.visible(app))

        let firstCell = app.cells.matching(identifier: "PocketsOverviewCell_0").firstMatch
        let firstCellExists = firstCell.waitForExistence(timeout: 5)
        XCTAssertTrue(firstCellExists)

        // Show pocket details screen.
        firstCell.tap()

        let detailsView = app.otherElements.matching(identifier: "PocketDetailsViewController").firstMatch
        let detailsViewExists = detailsView.waitForExistence(timeout: 5)
        XCTAssertTrue(detailsViewExists)

        let withdrawMoneyButton = app.staticTexts.matching(identifier: "Withdraw").firstMatch
        let withdrawMoneyButtonExists = withdrawMoneyButton.waitForExistence(timeout: 5)
        XCTAssertTrue(withdrawMoneyButtonExists)

        // Show withdraw money screen
        withdrawMoneyButton.forceTapElement()

        let withdrawMoneyView = app.otherElements.matching(identifier: "WithdrawMoneyViewController").firstMatch
        let withdrawMoneyViewExists = withdrawMoneyView.waitForExistence(timeout: 5)
        XCTAssertTrue(withdrawMoneyViewExists)

        let withdrawMoneyContinueButton = app.buttons.matching(identifier: "TransferMoneyContinueButton").firstMatch
        let withdrawMoneyContinueButtonExists = withdrawMoneyContinueButton.waitForExistence(timeout: 5)
        XCTAssertTrue(withdrawMoneyContinueButtonExists)

        // Select account field to show account selector
        let accountSelectorButton = app.buttons.matching(identifier: "paymentPartyViewButtonID").firstMatch
        let accountSelectorButtonExists = accountSelectorButton.waitForExistence(timeout: 5)
        XCTAssertTrue(accountSelectorButtonExists)

        // Show account selector screen
        accountSelectorButton.tap()

        let accountSelectorView = app.otherElements.matching(identifier: "AccountSelectorViewController").firstMatch
        let accountSelectorViewExits = accountSelectorView.waitForExistence(timeout: 5)
        XCTAssertTrue(accountSelectorViewExits)

        let firstAccount = app.otherElements.matching(identifier: "AccountItemView_0").firstMatch
        let firstAccountExits = firstAccount.waitForExistence(timeout: 5)
        XCTAssertTrue(firstAccountExits)

        // Select account to dismiss page
        firstAccount.tap()
        XCTAssertFalse(accountSelectorView.visible(app))

        // Verify right account is selected
        let accountName = app.staticTexts.matching(identifier: "Our joined account").firstMatch
        let accountNameExists = accountName.waitForExistence(timeout: 5)
        XCTAssertTrue(accountNameExists)

        // Enter amount
        let amountInput = app.textFields.matching(identifier: "AmountInputViewAmountInput.textInput.textField").firstMatch
        amountInput.forceTapElement()
        amountInput.typeText("12.3.45")
        XCTAssertEqual(amountInput.value as? String, "12.34")

        let doneButton = app.toolbars.buttons.matching(identifier: "Done").firstMatch
        doneButton.tap()

        // Click continue button and dismiss withdraw money screen
        withdrawMoneyContinueButton.tap()

        // Verify withdraw money success screen presented
        let successView = app.otherElements.matching(identifier: "SuccessViewController").firstMatch
        let successViewExists = successView.waitForExistence(timeout: 5)
        XCTAssertTrue(successViewExists)

        // Click continue button to open pocket details
        let successContinueButton = app.buttons.matching(identifier: "SuccessViewControllerContinueButton").firstMatch
        successContinueButton.tap()

        // Verify pocket details opened
        let detailsViewExists2 = detailsView.waitForExistence(timeout: 5)
        XCTAssertTrue(detailsViewExists2)
    }

    private func todaysDay(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        return dateFormatter.string(from: date)
    }

    private func dateString(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: date)
    }
}

extension XCUIElement {
    func visible(_ app: XCUIApplication) -> Bool {
        guard self.exists && !self.frame.isEmpty else { return false }
        return app.windows.element(boundBy: 0).frame.contains(self.frame)
    }

    func forceTapElement() {
        if isHittable {
            tap()
        } else {
            coordinate(withNormalizedOffset: CGVector()).tap()
        }
    }
}
