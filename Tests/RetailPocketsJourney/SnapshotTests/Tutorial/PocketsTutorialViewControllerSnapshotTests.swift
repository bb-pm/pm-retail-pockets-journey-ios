//
//  Created by Backbase R&D B.V. on 29/01/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
@testable import RetailPocketsJourney

final class PocketsTutorialViewControllerSnapshotTests: SnapshotTestCase {
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        do {
            try Backbase.initialize("config-stable.json", forceDecryption: false)
        } catch {
            XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
        }

        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testTutorialScreen() {
        let viewController = Tutorial.build(navigationController: .init())
        let navController = UINavigationController(rootViewController: viewController)
        verifySnapshots(withName: "tutorial", for: navController).forEach { XCTFail($0) }
    }
}
