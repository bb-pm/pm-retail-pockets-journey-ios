//
//  Created by Backbase R&D B.V. on 19/02/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
@testable import RetailPocketsJourney

final class CreatePocketReviewViewControllerSnapshotTests: SnapshotTestCase {
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testCreatePocketReview() {
        let navigationController = UINavigationController()
        let currency = Currency(amount: "1234", currencyCode: "USD")
        let deadline = Date(timeIntervalSince1970: 997654327)
        let goalRequestParams = CreatePocketGoalRequestParams(amountCurrency: currency, deadline: deadline)
        let requestParams = CreatePocketPostRequestParams(name: "Home", icon: "home", goal: goalRequestParams)
        let params = CreatePocketReview.EntryParams(createPocketPostRequestParams: requestParams)
        let viewController = CreatePocketReview.build(navigationController: navigationController, entryParams: params)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "create-pocket-review", for: navigationController).forEach { XCTFail($0) }
    }

    func testCreatePocketReviewWithoutGoal() {
        let navigationController = UINavigationController()
        let requestParams = CreatePocketPostRequestParams(name: "Home", icon: "home")
        let params = CreatePocketReview.EntryParams(createPocketPostRequestParams: requestParams)
        let viewController = CreatePocketReview.build(navigationController: navigationController, entryParams: params)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "create-pocket-review-without-goal", for: navigationController).forEach { XCTFail($0) }
    }
}
