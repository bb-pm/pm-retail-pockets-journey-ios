//
//  Created by Backbase R&D B.V. on 19/02/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
@testable import RetailPocketsJourney

final class CreatePocketGoalViewControllerSnapshotTests: SnapshotTestCase {
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testCreateGoal() {
        let navigationController = UINavigationController()
        let params = CreatePocketGoal.EntryParams(createPocketPostRequestParams: nil)
        let viewController = CreatePocketGoal.build(navigationController: navigationController, entryParams: params)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "create-pocket-goal", for: navigationController).forEach { XCTFail($0) }
    }

    func testCreateGoalSwitchOn() {
        var config = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD", presentableDescription: "$"))
        let date = Date(timeIntervalSince1970: 997654327)
        let datePicker = config.createPocketGoal.design.styles.datePicker
        config.createPocketGoal.design.styles.datePicker = {
            datePicker($0)
            $0.date = date
            $0.minimumDate = date
        }
        Resolver.register { config }

        let navigationController = UINavigationController()
        let params = CreatePocketGoal.EntryParams(createPocketPostRequestParams: nil)
        guard let viewController = CreatePocketGoal.build(navigationController: navigationController, entryParams: params) as? CreatePocketGoalViewController else { return }
        viewController.setSwitchOn()
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "create-pocket-goal-switch-on", for: navigationController).forEach { XCTFail($0) }
    }
}
