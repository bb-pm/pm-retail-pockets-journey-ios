//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Backbase
import ClientCommon
import PaymentOrderClient2
import RetailPocketsJourney
import Resolver

final public class MockPaymentsServiceUseCase: RetailPocketsJourney.PaymentsServiceUseCase {

    public init() {}

    public func postPaymentOrder(_ paymentOrder: RetailPocketsJourney.PaymentOrdersPost,
                                 completion: @escaping (Result<RetailPocketsJourney.PaymentOrdersPostResponse,
                                                               RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            let paymentOrderClient = PaymentOrderClient2.PaymentOrdersPost(from: paymentOrder)
            try client.postPaymentOrdersCall(paymentOrdersPost: paymentOrderClient, X_MFA: nil)
                .execute({ result in
                    switch result {
                    case let .success(response):
                        if let body = response.body {
                            let item = RetailPocketsJourney.PaymentOrdersPostResponse(from: body)
                            completion(.success(item))
                        } else {
                            completion(.failure(RetailPocketsJourney.ErrorResponse(statusCode: 0, data: nil, error: Pockets.Error.invalidResponse)))
                        }
                    case let .failure(errorResponse):
                        completion(.failure(RetailPocketsJourney.ErrorResponse(errorResponse)))
                    }
                })
        } catch {
            let errorCode = (error as NSError).code
            let clientErrorResponse = ClientCommon.ErrorResponse.error(errorCode, nil, error)
            completion(.failure(RetailPocketsJourney.ErrorResponse(clientErrorResponse)))
        }
    }

    private lazy var client: PaymentOrdersAPIProtocol = {
        if let dbsClient = Backbase.registered(client: PaymentOrdersAPI.self),
           let client = dbsClient as? PaymentOrdersAPI
        {
            return client
        } else if let client = Resolver.optional(PaymentOrdersAPIProtocol.self, name: nil, args: nil) {
            return client
        } else if let client = Resolver.optional(PaymentOrdersAPI.self, name: nil, args: nil) {
            return client
        } else {
            guard let serverURL = URL(string: Backbase.configuration().backbase.serverURL) else {
                fatalError("Invalid or no serverURL found in the SDK configuration.")
            }

            let newServerURL = serverURL
                .appendingPathComponent("api")
                .appendingPathComponent("payment-order-service")

            let client = PaymentOrdersAPI()
            client.baseURL = serverURL
            if let dataProvider = Resolver.optional(DBSDataProvider.self) {
                client.dataProvider = dataProvider
                return client
            } else {
                try? Backbase.register(client: client)
                guard let dbsClient = Backbase.registered(client: PaymentOrdersAPI.self),
                      let client = dbsClient as? PaymentOrdersAPI
                else {
                    fatalError("Failed to retrieve Payments client")
                }
                return client
            }
        }
    }()
}

extension PaymentOrderClient2.PaymentOrdersPost {
    init(from paymentOrder: RetailPocketsJourney.PaymentOrdersPost) {
        let transactionInfo = PaymentOrderClient2.InitiateTransaction.init(from: paymentOrder.transferTransactionInformation)
        self.init(originatorAccount: PaymentOrderClient2.AccountIdentification.init(from: paymentOrder.originatorAccount),
                  batchBooking: paymentOrder.batchBooking,
                  instructionPriority: PaymentOrderClient2.InstructionPriority.init(from: paymentOrder.instructionPriority),
                  requestedExecutionDate: paymentOrder.requestedExecutionDate,
                  paymentMode: PaymentOrderClient2.PaymentMode.init(from: paymentOrder.paymentMode),
                  paymentType: paymentOrder.paymentType,
                  schedule: PaymentOrderClient2.Schedule.init(from: paymentOrder.schedule),
                  entryClass: paymentOrder.entryClass,
                  transferTransactionInformation: transactionInfo,
                  approved: paymentOrder.approved,
                  additions: paymentOrder.additions)
    }
}

extension PaymentOrderClient2.AccountIdentification {
    init(from accountIdentification: RetailPocketsJourney.AccountIdentification) {
        let identification = PaymentOrderClient2.Identification.init(from: accountIdentification.identification)
        self.init(identification: identification, name: accountIdentification.name)
    }
}

extension PaymentOrderClient2.Identification {
    init(from identification: RetailPocketsJourney.Identification) {
        let schemeName = PaymentOrderClient2.SchemeNames.init(from: identification.schemeName)
        self.init(identification: identification.identification, schemeName: schemeName)
    }
}

extension PaymentOrderClient2.SchemeNames {
    init(from schemeName: RetailPocketsJourney.SchemeNames) {
        switch schemeName {
        case .bban: self = .bban
        case .iban: self = .iban
        case .id: self = .id
        case .externalId: self = .externalId
        case .email: self = .email
        case .mobile: self = .mobile
        @unknown default: self = .id
        }
    }
}

extension PaymentOrderClient2.InstructionPriority {
    init?(from instructionPriority: RetailPocketsJourney.InstructionPriority?) {
        guard let instructionPriority = instructionPriority else { return nil }
        switch instructionPriority {
        case .norm: self = .norm
        case .high: self = .high
        @unknown default: self = .norm
        }
    }
}

extension PaymentOrderClient2.PaymentMode {
    init?(from paymentMode: RetailPocketsJourney.PaymentMode?) {
        guard let paymentMode = paymentMode else { return nil }
        switch paymentMode {
        case .recurring: self = .recurring
        case .single: self = .single
        @unknown default: self = .single
        }
    }
}

extension PaymentOrderClient2.Schedule {
    init?(from schedule: RetailPocketsJourney.Schedule?) {
        guard let schedule = schedule else { return nil }
        let strategy = PaymentOrderClient2.Schedule.NonWorkingDayExecutionStrategy.init(from: schedule.nonWorkingDayExecutionStrategy)
        let transferFrequency = PaymentOrderClient2.Schedule.TransferFrequency.init(from: schedule.transferFrequency)
        self.init(nonWorkingDayExecutionStrategy: strategy,
                  transferFrequency: transferFrequency,
                  on: schedule.onDayNumber,
                  startDate: schedule.startDate,
                  endDate: schedule.endDate,
                  _repeat: schedule.repeat,
                  every: PaymentOrderClient2.Schedule.Every.init(from: schedule.every),
                  nextExecutionDate: schedule.nextExecutionDate)
    }
}

extension PaymentOrderClient2.Schedule.NonWorkingDayExecutionStrategy {
    init?(from nonWorkingDayExecutionStrategy: RetailPocketsJourney.Schedule.NonWorkingDayExecutionStrategy?) {
        guard let nonWorkingDayExecutionStrategy = nonWorkingDayExecutionStrategy else { return nil }
        switch nonWorkingDayExecutionStrategy {
        case .after: self = .after
        case .before: self = .before
        case .none: self = ._none
        @unknown default: self = ._none
        }
    }
}

extension PaymentOrderClient2.Schedule.TransferFrequency {
    init(from transferFrequency: RetailPocketsJourney.Schedule.TransferFrequency) {
        switch transferFrequency {
        case .biweekly: self = .biweekly
        case .once: self = .once
        case .daily: self = .daily
        case .weekly: self = .weekly
        case .monthly: self = .monthly
        case .quarterly: self = .quarterly
        case .yearly: self = .yearly
        @unknown default: fatalError()
        }
    }
}

extension PaymentOrderClient2.Schedule.Every {
    init(from every: RetailPocketsJourney.Schedule.Every) {
        switch every {
        case .one: self = ._1
        case .two: self = ._2
        @unknown default: fatalError()
        }
    }
}

extension PaymentOrderClient2.InitiateTransaction {
    init(from initiateTransaction: RetailPocketsJourney.InitiateTransaction) {
        self.init(counterparty: PaymentOrderClient2.InvolvedParty.init(from: initiateTransaction.counterparty),
                  counterpartyAccount: PaymentOrderClient2.InitiateCounterpartyAccount.init(from: initiateTransaction.counterpartyAccount),
                  counterpartyBank: PaymentOrderClient2.Bank.init(from: initiateTransaction.counterpartyBank),
                  instructedAmount: PaymentOrderClient2.Currency.init(from: initiateTransaction.instructedAmount),
                  correspondentBank: PaymentOrderClient2.Bank.init(from: initiateTransaction.correspondentBank),
                  intermediaryBank: PaymentOrderClient2.Bank.init(from: initiateTransaction.intermediaryBank),
                  messageToBank: initiateTransaction.messageToBank,
                  targetCurrency: initiateTransaction.targetCurrency,
                  remittanceInformation: initiateTransaction.remittanceInformation,
                  endToEndIdentification: initiateTransaction.endToEndIdentification,
                  mandateIdentifier: initiateTransaction.mandateIdentifier,
                  chargeBearer: PaymentOrderClient2.ChargeBearer.init(from: initiateTransaction.chargeBearer),
                  transferFee: PaymentOrderClient2.Currency.init(from: initiateTransaction.transferFee),
                  purposeOfPayment: PaymentOrderClient2.PurposeOfPayment.init(from: initiateTransaction.purposeOfPayment),
                  additions: initiateTransaction.additions)
    }
}

extension PaymentOrderClient2.InvolvedParty {
    init(from involvedParty: RetailPocketsJourney.InvolvedParty) {
        self.init(name: involvedParty.name,
                  role: PaymentOrderClient2.InvolvedPartyRole.init(from: involvedParty.role),
                  postalAddress: PaymentOrderClient2.PostalAddress.init(from: involvedParty.postalAddress),
                  recipientId: involvedParty.recipientId)
    }
}

extension PaymentOrderClient2.InvolvedPartyRole {
    init?(from involvedPartyRole: RetailPocketsJourney.InvolvedPartyRole?) {
        guard let involvedPartyRole = involvedPartyRole else { return nil }
        switch involvedPartyRole {
        case .creditor: self = .creditor
        case .debtor: self = .debtor
        @unknown default: fatalError()
        }
    }
}

extension PaymentOrderClient2.PostalAddress {
    init?(from postalAddress: RetailPocketsJourney.PostalAddress?) {
        guard let postalAddress = postalAddress else { return nil }
        self.init(addressLine1: postalAddress.addressLine1,
                  addressLine2: postalAddress.addressLine2,
                  streetName: postalAddress.streetName,
                  postCode: postalAddress.postCode,
                  town: postalAddress.town,
                  countrySubDivision: postalAddress.countrySubDivision,
                  country: postalAddress.countrySubDivision)
    }
}

extension PaymentOrderClient2.InitiateCounterpartyAccount {
    init(from initiateCounterpartyAccount: RetailPocketsJourney.InitiateCounterpartyAccount) {
        self.init(accountType: initiateCounterpartyAccount.accountType,
                  selectedContact: PaymentOrderClient2.SelectedContactDto.init(from: initiateCounterpartyAccount.selectedContact),
                  identification: PaymentOrderClient2.Identification.init(from: initiateCounterpartyAccount.identification),
                  name: initiateCounterpartyAccount.name)
    }
}

extension PaymentOrderClient2.SelectedContactDto {
    init?(from selectedContactDto: RetailPocketsJourney.SelectedContactDto?) {
        guard let selectedContactDto = selectedContactDto else { return nil }
        self.init(contactId: selectedContactDto.contactId, accountId: selectedContactDto.accountId)
    }
}

extension PaymentOrderClient2.Bank {
    init?(from bank: RetailPocketsJourney.Bank?) {
        guard let bank = bank else { return nil }
        self.init(bankBranchCode: bank.bankBranchCode,
                  name: bank.name,
                  postalAddress: PaymentOrderClient2.PostalAddress.init(from: bank.postalAddress),
                  bic: bank.bic)
    }
}

extension PaymentOrderClient2.Currency {
    init?(from currency: RetailPocketsJourney.Currency?) {
        guard let currency = currency else { return nil }
        self.init(from: currency)
    }

    init(from currency: RetailPocketsJourney.Currency) {
        self.init(amount: currency.amount,
                  currencyCode: currency.currencyCode,
                  additions: currency.additions)
    }
}

extension PaymentOrderClient2.ChargeBearer {
    init?(from chargeBearer: RetailPocketsJourney.ChargeBearer?) {
        guard let chargeBearer = chargeBearer else { return nil }
        switch chargeBearer {
        case .ben: self = .ben
        case .our: self = .our
        case .sha: self = .sha
        @unknown default: fatalError()
        }
    }
}

extension PaymentOrderClient2.PurposeOfPayment {
    init?(from purposeOfPayment: RetailPocketsJourney.PurposeOfPayment?) {
        guard let purposeOfPayment = purposeOfPayment else { return nil }
        self.init(code: purposeOfPayment.code, freeText: purposeOfPayment.freeText)
    }
}

extension RetailPocketsJourney.PaymentOrdersPostResponse {
    init(from paymentOrdersPostResponse: PaymentOrderClient2.PaymentOrdersPostResponse) {
        let rateInfo = RetailPocketsJourney.ExchangeRateInformation.init(from: paymentOrdersPostResponse.exchangeRateInformation)
        self.init(identifier: paymentOrdersPostResponse.id,
                  status: RetailPocketsJourney.Status.init(from: paymentOrdersPostResponse.status),
                  bankStatus: paymentOrdersPostResponse.bankStatus,
                  reasonCode: paymentOrdersPostResponse.reasonCode,
                  reasonText: paymentOrdersPostResponse.reasonText,
                  errorDescription: paymentOrdersPostResponse.errorDescription,
                  nextExecutionDate: paymentOrdersPostResponse.nextExecutionDate,
                  paymentSetupId: paymentOrdersPostResponse.paymentSetupId,
                  paymentSubmissionId: paymentOrdersPostResponse.paymentSubmissionId,
                  approvalStatus: paymentOrdersPostResponse.approvalStatus,
                  transferFee: RetailPocketsJourney.Currency.init(from: paymentOrdersPostResponse.transferFee),
                  exchangeRateInformation: rateInfo,
                  additions: paymentOrdersPostResponse.additions)
    }
}

extension RetailPocketsJourney.Status {
    // swiftlint:disable:next cyclomatic_complexity
    init(from status: PaymentOrderClient2.Status) {
        switch status {
        case .accepted: self = .accepted
        case .draft: self = .draft
        case .entered: self = .entered
        case .ready: self = .ready
        case .processed: self = .processed
        case .rejected: self = .rejected
        case .cancelled: self = .cancelled
        case .cancellationPending: self = .cancellationPending
        case .confirmationPending: self = .confirmationPending
        case .confirmationDeclined: self = .confirmationDeclined
        @unknown default: fatalError()
        }
    }
}

extension RetailPocketsJourney.ExchangeRateInformation {
    init?(from exchangeRateInformation: PaymentOrderClient2.ExchangeRateInformation?) {
        guard let exchangeRateInformation = exchangeRateInformation else { return nil }
        self.init(currencyCode: exchangeRateInformation.currencyCode,
                  rate: exchangeRateInformation.rate,
                  rateType: RetailPocketsJourney.ExchangeRateInformation.RateType.init(from: exchangeRateInformation.rateType),
                  contractIdentification: exchangeRateInformation.contractIdentification)
    }
}

extension RetailPocketsJourney.ExchangeRateInformation.RateType {
    init?(from rateType: PaymentOrderClient2.ExchangeRateInformation.RateType?) {
        guard let rateType = rateType else { return nil }
        switch rateType {
        case .actual: self = .actual
        case .indicative: self = .indicative
        case .agreed: self = .agreed
        @unknown default: fatalError()
        }
    }
}

extension RetailPocketsJourney.Currency {
    init?(from currency: PaymentOrderClient2.Currency?) {
        guard let currency = currency else { return nil }
        self.init(from: currency)
    }

    init(from currency: PaymentOrderClient2.Currency) {
        self.init(amount: currency.amount,
                  currencyCode: currency.currencyCode,
                  additions: currency.additions)
    }
}

