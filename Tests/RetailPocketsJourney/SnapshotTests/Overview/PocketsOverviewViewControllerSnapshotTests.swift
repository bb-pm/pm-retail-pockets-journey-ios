//
//  Created by Backbase R&D B.V. on 29/01/2021.
//

import Backbase
import Resolver
import SnapshotTesting
import XCTest
import PocketsClient2
@testable import RetailPocketsJourney

final class PocketsOverviewViewControllerSnapshotTests: SnapshotTestCase {
    var client: MockPocketsClient!
    
    override func setUp() {
        super.setUp()

        // uncomment this line to take new references
        // SnapshotTesting.isRecording = true

        do {
            try Backbase.initialize("config-stable.json", forceDecryption: false)
        } catch {
            XCTFail("Failed to initialise SDK: \(error.localizedDescription)")
        }

        client = MockPocketsClient()
        Resolver.register { self.client as PocketTailorClientAPIProtocol }
        Resolver.register { MockPocketsServiceUseCase() as PocketsServiceUseCase }
        Resolver.register { Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                             presentableDescription: "$")) }
    }

    func testEmptyScreen() {
        // on `verifySnapshots()` [SnapshotTestCase.swift] calls `viewWillAppear` for each device
        // so we need 15 `.success` responses
        client.listPocketsCallMocks = Array(repeating: .init(
                                                expectedResult: .success(
                                                    .init(statusCode: 200, header: [:], body: PocketListResponse(pockets: [])))),
                                            count: 15) // a number of snapshot testing devices
        let navigationController = UINavigationController()
        let viewController = Overview.build(navigationController: navigationController)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "overview-empty", for: navigationController).forEach { XCTFail($0) }
    }

    func testNotEmptyScreen() {
        let dec2030 = Date(timeIntervalSinceReferenceDate: 60 * 60 * 24 * 365 * 30) // Dec 25, 2030

        let goal = PocketsClient2.PocketGoal(amountCurrency: .init(amount: "1500", currencyCode: "USD"), deadline: dec2030, progress: 10)
        let balance = PocketsClient2.Currency(amount: "150", currencyCode: "USD")

        let goalCompleted = PocketsClient2.PocketGoal(amountCurrency: .init(amount: "1500", currencyCode: "USD"), deadline: dec2030, progress: 100)
        let balanceCompleted = PocketsClient2.Currency(amount: "1500", currencyCode: "USD")

        let pockets = [
            Pocket(id: "0", arrangementId: "", name: "Amsterdam Trip", icon: "holidays", goal: goal, balance: balance),
            Pocket(id: "1", arrangementId: "", name: "Living room decor", icon: "rent", balance: balance),
            Pocket(id: "2", arrangementId: "", name: "Garden renovation", icon: "home", goal: goalCompleted, balance: balanceCompleted),
        ]

        client.listPocketsCallMocks = Array(repeating: .init(
                                                expectedResult: .success(
                                                    .init(statusCode: 200,
                                                          header: [:],
                                                          body: PocketsClient2.PocketListResponse(pockets: pockets)))),
                                            count: 15)

        let navigationController = UINavigationController()
        let viewController = Overview.build(navigationController: navigationController)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "overview-not-empty", for: navigationController).forEach { XCTFail($0) }
    }

    func testRequestFailureScreen() {
        client.listPocketsCallMocks = Array(repeating: .init(
                                                expectedResult: .success(
                                                    .init(statusCode: 500, header: [:], body: nil))),
                                            count: 15)
        let navigationController = UINavigationController()
        let viewController = Overview.build(navigationController: navigationController)
        navigationController.viewControllers = [viewController]
        verifySnapshots(withName: "overview-failure", for: navigationController).forEach { XCTFail($0) }
    }

    func testNoInternetConnectionScreen() {
        let error = NSError(domain: NSURLErrorDomain as String, code: NSURLErrorNotConnectedToInternet, userInfo: nil) as Error
        client.listPocketsCallMocks = Array(repeating: .init(
                                                expectedResult: .failure(
                                                    .error(NSURLErrorNotConnectedToInternet, nil, error))),
                                            count: 15)

        let navigationController = UINavigationController()
        let viewController = Overview.build(navigationController: navigationController)
        navigationController.viewControllers = [viewController]

        verifySnapshots(withName: "overview-not-connected", for: viewController).forEach { XCTFail($0) }
    }
}
