//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Foundation
import XCTest
import ArrangementsClient2
import ClientCommon
import Backbase
import Resolver
import RetailPocketsJourney
@testable import RetailPocketsJourneyArrangementsUseCase

class ArrangementsUseCaseTests: XCTestCase {
    var client: MockArrangementsClient!
    var arrangementsUseCase: ArrangementsServiceUseCase!

    override func setUpWithError() throws {
        try super.setUpWithError()

        try Backbase.initialize("config-stable.json", forceDecryption: false)
        client = MockArrangementsClient()
        arrangementsUseCase = ArrangementsUseCase(client: client)
        Resolver.register { self.client as ProductSummaryAPIProtocol }
    }

    override func tearDown() {
        super.tearDown()

        Resolver.reset()
    }

    func test_givenAListOfArrangements_whenGetArrangementsCalled_thenTheResultShouldBeSuccessfull() {
        client.arrangementsCallMocks = client.successfulArrangementsMocks
        let expectation = XCTestExpectation()
        let params = GetArrangementsRequestParams(businessFunction: "somefunction", resourceName: "somename", privilege: "someprivilege")
        arrangementsUseCase.getArrangements(requestParams: params) { (result) in
            switch result {
            case .success(let arrangements):
                XCTAssertEqual(arrangements.count, 11)
                let first = arrangements.first
                XCTAssertNotNil(first)
                XCTAssertEqual(first?.identifier, "1cdb2224-8926-4b4d-a99f-1c9dfbbb4691")
                XCTAssertEqual(first?.externalArrangementId, "bcf10f4d-4b2f-4413-9bab-31ff693608b5")
                XCTAssertEqual(first?.externalLegalEntityId, "c7a382786d514262b75ab9531b749a2b")
                XCTAssertEqual(first?.externalProductId, "fade7867-533e-465e-90cb-e41675c54400")
                XCTAssertEqual(first?.name, "Mr and Mrs J. Smith")
                XCTAssertEqual(first?.bankAlias, "Our joined account")
                XCTAssertEqual(first?.sourceId, "LJBASI2XXXX")
                XCTAssertEqual(first?.bookedBalance, 1000.0)
                XCTAssertEqual(first?.availableBalance, 1500.0)
                XCTAssertEqual(first?.creditLimit, 442.12)
                XCTAssertEqual(first?.IBAN, "CY3887370130MJFTJ3B8Y9W7IGRO")
                XCTAssertEqual(first?.BBAN, "K8873701303897")
                XCTAssertEqual(first?.currency, "USD")
                XCTAssertEqual(first?.externalTransferAllowed, true)
                XCTAssertEqual(first?.urgentTransferAllowed, true)
                XCTAssertEqual(first?.accruedInterest, 0.54)
                XCTAssertEqual(first?.number, "PANS")
                XCTAssertEqual(first?.principalAmount, 620.54)
                XCTAssertEqual(first?.currentInvestmentValue, 0.16)
                XCTAssertEqual(first?.legalEntityIds.description, "[\"257da57a-11e4-4553-9175-54baf755069b\", \"cd83683b-13f2-43d8-882b-39c9ab27d499\"]")
                XCTAssertEqual(first?.productId, "36c8fc42-ec97-4f83-8a7c-d622625007f3")
                XCTAssertEqual(first?.productNumber, "ffdd939c-ac4a-4441-ae47-70a7259899e7")
                XCTAssertEqual(first?.productKindName, "Current Account")
                XCTAssertEqual(first?.productTypeName, "Current Account")
                XCTAssertEqual(first?.BIC, "AABAFI22")
                XCTAssertEqual(first?.bankBranchCode, "bankBranchCode")
                XCTAssertEqual(first?.accountOpeningDate?.description, "2016-01-28 16:41:41 +0000")
                XCTAssertEqual(first?.accountInterestRate, 100.2)
                XCTAssertEqual(first?.valueDateBalance, 100.1)
                XCTAssertEqual(first?.creditLimitUsage, 100.3)
                XCTAssertEqual(first?.creditLimitInterestRate, 100.4)
                XCTAssertEqual(first?.creditLimitExpiryDate?.description, "2019-09-28 16:41:41 +0000")
                XCTAssertEqual(first?.startDate?.description, "2016-02-28 16:41:41 +0000")
                XCTAssertEqual(first?.termUnit?.rawValue, "Y")
                XCTAssertEqual(first?.termNumber, 50.0)
                XCTAssertEqual(first?.interestPaymentFrequencyUnit?.rawValue, "M")
                XCTAssertEqual(first?.interestPaymentFrequencyNumber, 15.0)
                XCTAssertEqual(first?.maturityDate?.description, "2017-02-28 16:41:41 +0000")
                XCTAssertEqual(first?.maturityAmount, 99.5)
                XCTAssertEqual(first?.autoRenewalIndicator, true)
                XCTAssertEqual(first?.interestSettlementAccount, "interestSettlementAccount1")
                XCTAssertEqual(first?.outstandingPrincipalAmount, 100.2)
                XCTAssertEqual(first?.monthlyInstalmentAmount, 100.1)
                XCTAssertEqual(first?.amountInArrear, 100.3)
                XCTAssertEqual(first?.minimumRequiredBalance, 80.4)
                XCTAssertEqual(first?.creditCardAccountNumber, "123456")
                XCTAssertEqual(first?.validThru?.description, "2019-02-28 16:41:41 +0000")
                XCTAssertEqual(first?.applicableInterestRate, 101.2)
                XCTAssertEqual(first?.remainingCredit, 50.0)
                XCTAssertEqual(first?.outstandingPayment, 105.5)
                XCTAssertEqual(first?.minimumPayment, 51.1)
                XCTAssertEqual(first?.minimumPaymentDueDate?.description, "2018-02-28 16:41:41 +0000")
                XCTAssertEqual(first?.totalInvestmentValue, 110.2)
                XCTAssertEqual(first?.debitCards.first?.cardId, "id1")
                XCTAssertEqual(first?.debitCards.first?.cardStatus, "Active")
                XCTAssertEqual(first?.debitCards.first?.cardType, "Visa Electron")
                XCTAssertEqual(first?.debitCards.first?.cardholderName, "John Doe")
                XCTAssertEqual(first?.debitCards.first?.expiryDate, "2020-08-22")
                XCTAssertEqual(first?.debitCards.first?.number, "4578")
                XCTAssertEqual(first?.debitCards.first?.additions?.description, "[\"addition1\": \"This is an additional field\"]")
                XCTAssertEqual(first?.accountHolderAddressLine1, "accountHolderAddressLine11")
                XCTAssertEqual(first?.accountHolderAddressLine2, "accountHolderAddressLine12")
                XCTAssertEqual(first?.accountHolderStreetName, "accountHolderStreetName1")
                XCTAssertEqual(first?.town, "Paris")
                XCTAssertEqual(first?.postCode, "2000")
                XCTAssertEqual(first?.countrySubDivision, "countrySubDivision1")
                XCTAssertEqual(first?.accountHolderNames, "Danthe Mohr,Toso Malerot")
                XCTAssertEqual(first?.accountHolderCountry, "FR")
                XCTAssertEqual(first?.creditAccount, true)
                XCTAssertEqual(first?.debitAccount, true)
                XCTAssertEqual(first?.lastUpdateDate?.description, "2016-01-28 16:41:41 +0000")
                XCTAssertEqual(first?.userPreferences?.alias, "Our joined account")
                XCTAssertEqual(first?.userPreferences?.favorite, true)
                XCTAssertEqual(first?.userPreferences?.visible, true)
                XCTAssertEqual(first?.userPreferences?.additions?.description, "[\"addition2\": \"This is an additional field\"]")
                XCTAssertEqual(first?.product?.externalId, "externalProductidId")
                XCTAssertEqual(first?.product?.externalTypeId, "externalProductTypeId")
                XCTAssertEqual(first?.product?.typeName, "Current Account")
                XCTAssertEqual(first?.product?.productKind?.externalKindId, "kind1")
                XCTAssertEqual(first?.product?.productKind?.kindName, "Current Account")
                XCTAssertEqual(first?.product?.productKind?.identifier, 1)
                XCTAssertEqual(first?.product?.productKind?.additions?.description, "[\"addition4\": \"This is an additional field\"]")
                XCTAssertEqual(first?.product?.productKind?.kindUri, "current-account")
                XCTAssertEqual(first?.state?.externalStateId, "externalStateId1")
                XCTAssertEqual(first?.state?.additions?.description, "[\"addition5\": \"This is an additional field\"]")
                XCTAssertEqual(first?.parentId, "someid")
                XCTAssertEqual(first?.externalParentId, "someparentid")
                XCTAssertEqual(first?.financialInstitutionId, 1)
                XCTAssertEqual(first?.lastSyncDate?.description, "2020-09-28 16:41:41 +0000")
                XCTAssertEqual(first?.additions?.description, "[\"addition6\": \"This is an additional field\"]")
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenNoInternetAccessWithNSError_whenGetArrangementsCalled_ThenItShouldFailDueToNoInternet() {
        let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        client.arrangementsCallMocks = [.init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, error)))]
        let expectation = XCTestExpectation()
        let params = GetArrangementsRequestParams(businessFunction: "somefunction", resourceName: "somename", privilege: "someprivilege")
        arrangementsUseCase.getArrangements(requestParams: params) { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssertNotNil(error.error)
                XCTAssertEqual(error.statusCode, NSURLErrorNotConnectedToInternet)
                XCTAssertEqual(error.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.notConnected)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenNoInternetAccessWithCallError_whenGetArrangementsCalled_ThenItShouldFailDueToNoInternet() {
        let error = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        client.arrangementsCallMocks = [.init(expectedResult: .failure(.error(NSURLErrorNotConnectedToInternet, nil, CallError.unsuccessfulHTTPStatusCode(error))))]
        let expectation = XCTestExpectation()
        let params = GetArrangementsRequestParams(businessFunction: "somefunction", resourceName: "somename", privilege: "someprivilege")
        arrangementsUseCase.getArrangements(requestParams: params) { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssertNotNil(error.error)
                XCTAssertEqual(error.statusCode, NSURLErrorNotConnectedToInternet)
                XCTAssertEqual(error.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.notConnected)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenGeneralErrorWithNSError_whenGetArrangementsCalled_ThenItShouldFailDueToLoadingFailed() {
        let error = NSError(domain: "", code: 403, userInfo: nil)
        client.arrangementsCallMocks = [.init(expectedResult: .failure(.error(403, nil, error)))]
        let expectation = XCTestExpectation()
        let params = GetArrangementsRequestParams(businessFunction: "somefunction", resourceName: "somename", privilege: "someprivilege")
        arrangementsUseCase.getArrangements(requestParams: params) { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let responseError):
                XCTAssertNotNil(responseError)
                XCTAssertNotNil(responseError.error)
                XCTAssertEqual(responseError.statusCode, 403)
                XCTAssertEqual(responseError.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.loadingFailure(underlying: error))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenGeneralErrorWithCallError_whenGetArrangementsCalled_ThenItShouldFailDueToLoadingFailed() {
        let error = NSError(domain: "", code: 403, userInfo: nil)
        client.arrangementsCallMocks = [.init(expectedResult: .failure(.error(403, nil, CallError.unsuccessfulHTTPStatusCode(error))))]
        let expectation = XCTestExpectation()
        let params = GetArrangementsRequestParams(businessFunction: "somefunction", resourceName: "somename", privilege: "someprivilege")
        arrangementsUseCase.getArrangements(requestParams: params) { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let responseError):
                XCTAssertNotNil(responseError)
                XCTAssertNotNil(responseError.error)
                XCTAssertEqual(responseError.statusCode, 403)
                XCTAssertEqual(responseError.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.loadingFailure(underlying: error))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }

    func test_givenInvalidSuccessResponse_whenGetArrangementsCalled_ThenItShouldFailDueToInvalidResponse() {
        client.arrangementsCallMocks = [.init(expectedResult: .success(.init(statusCode: 201,
                                                                             header: [:],
                                                                             body: nil)))]
        let expectation = XCTestExpectation()
        let params = GetArrangementsRequestParams(businessFunction: "somefunction", resourceName: "somename", privilege: "someprivilege")
        arrangementsUseCase.getArrangements(requestParams: params) { (result) in
            switch result {
            case .success(_):
                XCTFail()
            case .failure(let responseError):
                XCTAssertNotNil(responseError)
                XCTAssertNotNil(responseError.error)
                XCTAssertEqual(responseError.statusCode, 0)
                XCTAssertEqual(responseError.error as? RetailPocketsJourney.Pockets.Error, RetailPocketsJourney.Pockets.Error.invalidResponse)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }
}
