# Journey starter project

## Getting started
You can clone or fork this repository to create your own journey project.

### Generating the project
Make sure that the `fastlane` submodule is initialized and up-to-date by running:

```
git submodule init
git submodule update
bundle install
bundle exec fastlane setup
```

or:

`git submodule init && git submodule update && bundle install && bundle exec fastlane setup`

This will generate:
- An Xcode project with the source code in the repo.

## Tests

Run test target on the iPhone 11 (iOS 14.0.1) simulator.

## Docker with Mocks

- https://backbase.atlassian.net/wiki/spaces/AT/pages/1836056766/iOS+Running+api-contract-based+tests+with+a+Prism+server+in+iOS+Widget+collection
- https://stash.backbase.com/projects/CTRLSDLC/repos/api-simulator/browse

```
docker run -v $(pwd)/config/openapi.yaml:/config/openapi.yaml --env-file ./config/env.list -p 14080:14080 --rm -it harbor.backbase.eu/staging/api-simulator:latest
```

Then change in TestResources/config-stable.json a serverURL value to `http://localhost:14080`

App's NSAllowsArbitraryLoads may also be needed.
