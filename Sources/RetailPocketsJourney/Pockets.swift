//
//  Created by Backbase R&D B.V. on 12/02/2020.
//

import UIKit

/// Top level object of the `Pockets` journey.
public struct Pockets {}

internal final class PocketsBundleToken {}
