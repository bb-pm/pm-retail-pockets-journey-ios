//
// Created by Backbase R&D B.V. on 03/05/2021.
//

import Foundation
import PaymentOrderClient2
import Backbase
import ClientCommon
import RetailPocketsJourney
import Resolver

/// Implementation for `PaymentsServiceUseCase` using `PaymentOrderClient2`.
final public class PaymentsUseCase: RetailPocketsJourney.PaymentsServiceUseCase {
    /// Initialiser for `PaymentsUseCase`.
    public init(client: PaymentOrdersAPIProtocol) {
        self.client = client
    }

    public func postPaymentOrder(_ paymentOrder: RetailPocketsJourney.PaymentOrdersPost,
                                 completion: @escaping (Result<RetailPocketsJourney.PaymentOrdersPostResponse,
                                                               RetailPocketsJourney.ErrorResponse>) -> Void) {
        do {
            try client.postPaymentOrdersCall(paymentOrdersPost: .init(from: paymentOrder), X_MFA: nil).execute { result in
                switch result {
                case .success(let response):
                    guard let body = response.body else {
                        let errorResponse = RetailPocketsJourney.ErrorResponse(statusCode: 0,
                                                                               data: nil,
                                                                               error: Pockets.Error.invalidResponse)
                        completion(.failure(errorResponse))
                        return
                    }
                    completion(.success(.init(from: body)))
                case .failure(let errorResponse):
                    completion(.failure(RetailPocketsJourney.ErrorResponse(errorResponse)))
                }
            }
        } catch {
            let errorCode = (error as NSError).code
            let clientErrorResponse = ClientCommon.ErrorResponse.error(errorCode, nil, error)
            completion(.failure(RetailPocketsJourney.ErrorResponse(clientErrorResponse)))
        }
    }

    private let client: PaymentOrdersAPIProtocol
}
