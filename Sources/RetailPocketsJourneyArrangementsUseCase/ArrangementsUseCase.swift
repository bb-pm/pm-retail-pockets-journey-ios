//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Backbase
import ClientCommon
import ArrangementsClient2
import RetailPocketsJourney
import Resolver

/// Implementation for `ArrangementsServiceUseCase` using `ArrangementsClient2`.
final public class ArrangementsUseCase: RetailPocketsJourney.ArrangementsServiceUseCase {
    /// Callback typealias for `GET` arrangements call.
    public typealias GetArrangementsResultHandler = (Result<[RetailPocketsJourney.ProductSummaryItem],
                                                            RetailPocketsJourney.ErrorResponse>) -> Void

    /// Initialiser for `ArrangementsUseCase`.
    public init(client: ProductSummaryAPIProtocol) {
        self.client = client
    }

    public func getArrangements(requestParams: GetArrangementsRequestParams, completion: @escaping GetArrangementsResultHandler) {
        do {
            let orderBy = requestParams.orderBy?.filter { $0.field != nil }.map(\.field!)
            let orderByValue = (orderBy != nil && orderBy!.isEmpty) ? nil : orderBy
            try client.getArrangementsByBusinessFunctionCall(businessFunction: requestParams.businessFunction,
                                                             resourceName: requestParams.resourceName,
                                                             privilege: requestParams.privilege,
                                                             contentLanguage: requestParams.contentLanguage,
                                                             withLatestBalances: requestParams.withLatestBalances,
                                                             maskIndicator: requestParams.maskIndicator,
                                                             debitAccount: requestParams.debitAccount,
                                                             creditAccount: requestParams.creditAccount,
                                                             externalTransferAllowed: requestParams.externalTransferAllowed,
                                                             productKindName: requestParams.productKindName,
                                                             legalEntityIds: requestParams.legalEntityIds,
                                                             sourceId: requestParams.sourceId,
                                                             favorite: requestParams.favorite,
                                                             searchTerm: requestParams.searchTerm,
                                                             customOrder: requestParams.customOrder,
                                                             favoriteFirst: requestParams.favoriteFirst,
                                                             from: requestParams.from, size: requestParams.size,
                                                             cursor: requestParams.cursor,
                                                             orderBy: orderByValue,
                                                             direction: requestParams.direction?.direction).execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        let items = body.map(RetailPocketsJourney.ProductSummaryItem.init)
                        completion(.success(items))
                    } else {
                        completion(.failure(RetailPocketsJourney.ErrorResponse(statusCode: 0,
                                                                               data: nil,
                                                                               error: Pockets.Error.invalidResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(RetailPocketsJourney.ErrorResponse(errorResponse)))
                }
            }
        } catch {
            let errorCode = (error as NSError).code
            let clientErrorResponse = ClientCommon.ErrorResponse.error(errorCode, nil, error)
            completion(.failure(RetailPocketsJourney.ErrorResponse(clientErrorResponse)))
        }
    }

    private let client: ProductSummaryAPIProtocol
}
