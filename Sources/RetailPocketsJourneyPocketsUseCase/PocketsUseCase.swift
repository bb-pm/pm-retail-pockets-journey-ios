//
// Created by Backbase R&D B.V. on 22/01/2021.
//

import Backbase
import ClientCommon
import PocketsClient2
import RetailPocketsJourney
import Resolver

/// Implementation for `PocketsServiceUseCase` using `PocketsClient2`.
final public class PocketsUseCase: RetailPocketsJourney.PocketsServiceUseCase {
    public typealias ClosePocketResultHandler = (Result<Void, RetailPocketsJourney.ErrorResponse>) -> Void
    public typealias ViewPocketResultHandler = (Result<RetailPocketsJourney.Pocket, RetailPocketsJourney.ErrorResponse>) -> Void
    public typealias CreatePocketResultHandler = (Result<RetailPocketsJourney.Pocket, RetailPocketsJourney.ErrorResponse>) -> Void
    public typealias ListPocketsResultHandler = (Result<RetailPocketsJourney.PocketListResponse,
                                                        RetailPocketsJourney.ErrorResponse>) -> Void

    /// Initialiser for `PocketsUseCase`.
    public init(client: PocketTailorClientAPIProtocol) {
        self.client = client
    }

    public func closePocket(params: ClosePocketRequestParams, completion: @escaping ClosePocketResultHandler) {
        do {
            try client.closePocketCall(closePocketRequest: params.params).execute { result in
                switch result {
                case .success:
                    completion(.success(()))
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    public func viewPocket(params: ViewPocketGetRequestParams, completion: @escaping ViewPocketResultHandler) {
        do {
            try client.viewPocketCall(pocketId: params.pocketId).execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        completion(.success(Pocket(body)))
                    } else {
                        completion(.failure(ErrorResponse(statusCode: 0, data: nil, error: Pockets.Error.invalidResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    public func createPocket(params: CreatePocketPostRequestParams, completion: @escaping CreatePocketResultHandler) {
        do {
            try client.postPocketCall(pocketPostRequest: params.params).execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        completion(.success(Pocket(body)))
                    } else {
                        completion(.failure(.init(statusCode: 0, data: nil, error: CallError.emptyDataResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(.init(errorResponse)))
                }
            }
        } catch {
            completion(.failure(.init(statusCode: 0, data: nil, error: error)))
        }
    }

    public func listPockets(completion: @escaping ListPocketsResultHandler) {
        do {
            try client.listPocketsCall().execute { result in
                switch result {
                case let .success(response):
                    if let body = response.body {
                        completion(.success(PocketListResponse(body)))
                    } else {
                        completion(.failure(ErrorResponse(statusCode: 0, data: nil, error: Pockets.Error.invalidResponse)))
                    }
                case let .failure(errorResponse):
                    completion(.failure(ErrorResponse(errorResponse)))
                }
            }
        } catch {
            completion(.failure(ErrorResponse(statusCode: 0, data: nil, error: error)))
        }
    }

    private let client: PocketTailorClientAPIProtocol
}

extension RetailPocketsJourney.ErrorResponse {

    init(statusCode: Int? = nil, data: Data? = nil, error: CallError) {
        self.init(statusCode: statusCode, data: data, error: Self.mapCommonClientError(error))
    }

    init(_ errorResponse: ClientCommon.ErrorResponse) {
        switch errorResponse {
        case let .error(statusCode, data, error as ClientCommon.CallError):
            self.init(statusCode: statusCode, data: data, error: Self.mapCommonClientError(error))
        case let .error(statusCode, data, error):
            self.init(statusCode: statusCode, data: data, error: Self.mapNsError(error: error) ?? error)
        default:
            self.init(statusCode: 0, data: nil, error: NSError())
        }
    }

    private static func mapCommonClientError(_ error: ClientCommon.CallError?) -> Error? {
        switch error {
        case .emptyDataResponse:
            return Pockets.Error.invalidResponse
        case .nilHTTPResponse(let detailError), .unsuccessfulHTTPStatusCode(let detailError):
            return Self.mapNsError(error: detailError)
        default:
            return Pockets.Error.loadingFailure(underlying: nil)
        }
    }

    private static func mapNsError(error: Swift.Error?) -> Pockets.Error? {
        guard let nsError = (error as NSError?),
              nsError.code == NSURLErrorNotConnectedToInternet ||
                nsError.domain == kCFErrorDomainCFNetwork as String
        else {
            return Pockets.Error.loadingFailure(underlying: error)
        }
        return Pockets.Error.notConnected
    }
}

extension ClosePocketRequestParams {
    var params: PocketsClient2.ClosePocketRequest {
        PocketsClient2.ClosePocketRequest(id: pocketId)
    }
}

extension RetailPocketsJourney.Pocket {
    init(_ pocket: PocketsClient2.Pocket) {
        self.init(identifier: pocket.id,
                  arrangementId: pocket.arrangementId,
                  name: pocket.name,
                  icon: pocket.icon,
                  goal: PocketGoal(pocket.goal),
                  balance: Currency(pocket.balance))
    }
}

extension RetailPocketsJourney.PocketGoal {
    init?(_ goal: PocketsClient2.PocketGoal?) {
        guard let goal = goal else { return nil }
        self.init(amountCurrency: Currency(goal.amountCurrency),
                  deadline: goal.deadline,
                  progress: goal.progress)
    }
}

extension RetailPocketsJourney.Currency {
    init(_ currency: PocketsClient2.Currency) {
        self.init(amount: currency.amount,
                  currencyCode: currency.currencyCode,
                  additions: currency.additions)
    }

    init?(_ currency: PocketsClient2.Currency?) {
        guard let currency = currency else { return nil }
        self.init(currency)
    }

    var params: PocketsClient2.Currency {
        PocketsClient2.Currency(amount: amount,
                                currencyCode: currencyCode,
                                additions: additions)
    }
}

extension RetailPocketsJourney.CreatePocketPostRequestParams {
    var params: PocketsClient2.PocketPostRequest {
        PocketsClient2.PocketPostRequest(name: name, icon: icon, goal: goal?.params)
    }
}

extension RetailPocketsJourney.PocketListResponse {
    init(_ response: PocketsClient2.PocketListResponse) {
        self.init(pockets: response.pockets.map(Pocket.init))
    }
}

extension RetailPocketsJourney.CreatePocketGoalRequestParams {
    var params: PocketsClient2.PocketGoalRequest {
        PocketsClient2.PocketGoalRequest(amountCurrency: amountCurrency?.params, deadline: deadline)
    }
}
