//
// Created by Backbase R&D B.V. on 2020. 05. 29..
//

import Foundation
import Backbase

class TestDataProvider: NSObject, DBSDataProvider {
    var testBehaviours: [TestBehavior]?

    init(testBehaviours: [TestBehavior]? = nil) {
        self.testBehaviours = testBehaviours
    }

    func execute(_ request: URLRequest, completionHandler: ((URLResponse?, Data?, Error?) -> Void)? = nil) {
        if let behaviour = testBehaviour(for: request), behaviour.type != .default {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                switch Response.create(from: request.url!, for: behaviour.type) {
                case .success(let data):
                    let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: ["x-total-count": "0"])!
                    completionHandler?(response, data, nil)
                case .error(let error):
                    let response = HTTPURLResponse(url: request.url!,
                                                   statusCode: (error as NSError).code,
                                                   httpVersion: "HTTP/1.1",
                                                   headerFields: nil)!
                    completionHandler?(response, nil, error)
                }
            }
        } else {
            try? saveTestBehaviour(for: request)
            URLSession(configuration: Backbase.securitySessionConfiguration()).dataTask(with: request) { data, response, error in
                DispatchQueue.main.async {
                    completionHandler?(response, data, error)
                }
            }.resume()
        }
    }

    private func saveTestBehaviour(for request: URLRequest) throws {
        let launchEnvironment = ProcessInfo.processInfo.environment
        guard launchEnvironment["RECORD_BEHAVIOURS"] != nil else {
            return
        }

        if let url = request.url,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
            let method = HTTPMethod(request: request) {
            var params: [String: String] = [:]
            components.queryItems?.forEach {
                if let value = $0.value {
                    params[$0.name] = value
                }
            }

            let conditions = BehaviourConditions(url: components.path, method: method, params: params)
            let testBehaivour = TestBehavior(given: conditions, then: [Behaviour()])
            let fileUrl = URL(fileURLWithPath: NSTemporaryDirectory())
                .appendingPathComponent(components.path)
                .appendingPathExtension("json")
            try FileManager().createDirectory(at: fileUrl.deletingLastPathComponent(),
                                               withIntermediateDirectories: true,
                                               attributes: nil)
            let encoder = JSONEncoder()
            if #available(iOS 13.0, *) {
                encoder.outputFormatting = .withoutEscapingSlashes
            }
            try encoder.encode(testBehaivour).write(to: fileUrl)
            print("TestBehaviour for \(url) saved at \(fileUrl)")
        }
    }

    private func testBehaviour(for urlRequest: URLRequest) -> Behaviour? {
        guard let method = HTTPMethod(request: urlRequest),
            let url = urlRequest.url,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return nil }

        let params = (components.queryItems ?? []).reduce([String: String]()) { (result, item) -> [String: String] in
            var result = result
            result[item.name] = item.value
            return result
        }

        guard let test = testBehaviours?.first(where: { behaviour in
            let matchingParams = params.reduce(true) { $0 && behaviour.given.params[$1.key] == $1.value }
            return behaviour.given.url == url.path && behaviour.given.method == method && matchingParams
        }) else { return nil }

        let behaviour = test.then.first { $0.times == nil || $0.times! > 0 }

        if let times = behaviour?.times {
            behaviour?.times = times - 1
        }

        return behaviour
    }
}

private enum Response {
    case error(Error)
    case success(Data?)

    static func create(from url: URL, for type: BehaviourResponseType) -> Response {
        switch type {
        case .empty:
            return .success("{}".data(using: .utf8))
        case .emptyList:
            return .success("[]".data(using: .utf8))
        case .failed:
            return .error(URLError.fileNotFound)
        case .noInternet:
            return .error(URLError.notConnectedToInternet)
        case .unauthorized:
            return .error(URLError.unauthorizedAccess)
        case .forbidden:
            return .error(URLError.forbidden)
        case .default:
            fatalError("default response should be handled by real network call")
        }
    }
}
