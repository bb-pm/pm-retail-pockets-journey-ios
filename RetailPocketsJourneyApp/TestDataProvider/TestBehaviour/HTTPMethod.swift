//
// Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import Foundation

enum HTTPMethod: String, Codable {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"

    init?(request: URLRequest) {
        guard let method = request.httpMethod else { return nil }
        self.init(rawValue: method)
    }
}

extension URLError {
    static let fileNotFound = NSError(domain: NSURLErrorDomain,
                                      code: 404,
                                      userInfo: [NSLocalizedDescriptionKey: "File not found."])

    static let badRequest = NSError(domain: NSURLErrorDomain,
                                    code: 400,
                                    userInfo: [NSLocalizedDescriptionKey: "Bad Request"])

    static let notConnectedToInternet = NSError(domain: NSURLErrorDomain,
                                                code: NSURLErrorNotConnectedToInternet,
                                                userInfo: [NSLocalizedDescriptionKey: "You are not connected to the internet."])

    static let unauthorizedAccess = NSError(domain: NSURLErrorDomain,
                                            code: 401,
                                            userInfo: [NSLocalizedDescriptionKey: "Unauthorized access."])

    static let forbidden = NSError(domain: NSURLErrorDomain,
                                   code: 403,
                                   userInfo: [NSLocalizedDescriptionKey: "Server cannot or will not process the request."])
}
