//
// Copyright © 2019 Backbase R&D B.V. All rights reserved.
//

enum BehaviourResponseType: String, Codable {
    case `default`
    case noInternet
    case empty
    case emptyList
    case failed
    case unauthorized
    case forbidden
}
