//
// Copyright © 2019 Backbase R&D B.V. All rights reserved.
//

class Behaviour: Codable {
    var type: BehaviourResponseType
    var times: Int?

    enum JSONKeys: String, CodingKey {
        case type = "response"
        case times
    }

    init(type: BehaviourResponseType = .default, times: Int = 1) {
        self.type = type
        self.times = times
    }
}
