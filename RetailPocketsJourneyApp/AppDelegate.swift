//
//  AppDelegate.swift
//  RetailPocketsJourney-App
//
//  Created by Backbase on 14/01/2021.
//

import UIKit
import Resolver
import Backbase
import BackbaseDesignSystem
import RetailPocketsJourney
import PocketsClient2
import RetailPocketsJourneyPocketsUseCase
import IdentityAuthenticationJourney
import ArrangementsClient2
import RetailPocketsJourneyArrangementsUseCase
import PaymentOrderClient2
import RetailPocketsJourneyPaymentsUseCase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var appConfiguration = Pockets.Configuration(currencyInfo: CurrencyInfo(currencyCode: "USD",
                                                                            presentableDescription: "$"))

    private lazy var pocketsClient: PocketTailorClientAPIProtocol = {
        guard let serverURL = URL(string: Backbase.configuration().backbase.serverURL) else {
            fatalError("Invalid or no serverURL found in the SDK configuration.")
        }

        let newServerURL = serverURL
            .appendingPathComponent("api")
            .appendingPathComponent("pocket-tailor")

        let client = PocketTailorClientAPI()
        client.baseURL = newServerURL
        if let dataProvider = Resolver.optional(DBSDataProvider.self) {
            client.dataProvider = dataProvider
            return client
        } else {
            try? Backbase.register(client: client)
            guard let dbsClient = Backbase.registered(client: PocketTailorClientAPI.self),
                  let client = dbsClient as? PocketTailorClientAPI
            else {
                fatalError("Failed to retrieve Pockets client")
            }
            return client
        }
    }()

    private lazy var arrangementsClient: ProductSummaryAPIProtocol = {
        guard let serverURL: URL = URL(string: Backbase.configuration().backbase.serverURL) else {
            fatalError("Invalid or no serverURL found in the SDK configuration.")
        }

        let newServerURL = serverURL
            .appendingPathComponent("api")
            .appendingPathComponent("arrangement-manager")

        if let dbsClient = Backbase.registered(client: ProductSummaryAPI.self),
            let client = dbsClient as? ProductSummaryAPI {
            return client
        } else if let client = Resolver.optional(ProductSummaryAPI.self) {
            return client
        } else {
            let client = ProductSummaryAPI()
            client.baseURL = newServerURL
            if let dataProvider = Resolver.optional(DBSDataProvider.self) {
                client.dataProvider = dataProvider
                return client
            } else {
                try? Backbase.register(client: client)
                guard let dbsClient = Backbase.registered(client: ProductSummaryAPI.self),
                    let client = dbsClient as? ProductSummaryAPI else {
                        fatalError("Failed to retrieve Arrangements client")
                }
                return client
            }
        }
    }()

    func clientFactory<T: DBSClient>(defaultClient: T, clientPath: String) -> T {
        if let dbsClient = Backbase.registered(client: T.self),
           let client = dbsClient as? T {
            return client
        } else if let client = Resolver.optional(T.self) {
            return client
        } else {
            guard let serverURL: URL = URL(string: Backbase.configuration().backbase.serverURL) else {
                fatalError("Invalid or no serverURL found in the SDK configuration.")
            }
            let client = defaultClient
            client.baseURL = serverURL.appendingPathComponent(clientPath)
            if let dataProvider = Resolver.optional(DBSDataProvider.self) {
                client.dataProvider = dataProvider
                return client
            } else {
                try? Backbase.register(client: client)
                guard let dbsClient = Backbase.registered(client: T.self),
                      let client = dbsClient as? T else {
                    fatalError("Failed to retrieve \(T.self) client")
                }
                return client
            }
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {

        initializeBackbase()
        configureJourney()

        window = UIWindow()
        let launchEnvironment = ProcessInfo.processInfo.environment
        if let value = launchEnvironment["DataProvider"], value == "local" {
            // NOTE: On local data testing, the tutorial screen always shows.
            let storage = Backbase.registered(plugin: EncryptedStorage.self) as? EncryptedStorage
            storage?.storageComponent.removeItem("com.backbase.retail.journey.pockets.isTutorialDone")
            Resolver.register { LocalDataProvider() as DBSDataProvider }
            let navigationController = UINavigationController()

            DesignSystem.shared.retailStyles.navigationBar(navigationController.navigationBar)
            if #available(iOS 13.0, *) {
                // keep empty for ios 13+
            } else {
                navigationController.navigationBar.setBackgroundImage(nil, for: .default) // fix for ios 12
            }

            let vc = Overview.build(navigationController: navigationController)
            navigationController.viewControllers = [vc]
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        } else {
            let dataProvider = testDataProvider()
            Resolver.register { dataProvider as DBSDataProvider }
            window?.rootViewController = UIViewController()
            window?.makeKeyAndVisible()
            authenticationUseCase.validateSession()
        }

        return true
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }

    // MARK: - Backbase SDK

    private func initializeBackbase() {
        do {
            if let useLatest = ProcessInfo.processInfo.environment["USE_LATEST_ENV"], useLatest == "YES" {
                try Backbase.initialize("config-latest.json", forceDecryption: false)
            } else {
                try Backbase.initialize("config-stable.json", forceDecryption: false)
            }
            Backbase.register(navigationEventListener: self, selector: #selector(handleNavigationEvent))
        } catch {
            fatalError("Backbase MSDK initialization failed: \(error.localizedDescription)")
        }
    }

    // MARK: - Initialize Journeys

    private lazy var pocketsUseCase = RetailPocketsJourneyPocketsUseCase.PocketsUseCase(client: self.pocketsClient)

    private lazy var arrangementsUseCase = RetailPocketsJourneyArrangementsUseCase.ArrangementsUseCase(client: self.arrangementsClient)

    private lazy var paymentsUseCase = RetailPocketsJourneyPaymentsUseCase.PaymentsUseCase(client: clientFactory(defaultClient: PaymentOrdersAPI(), clientPath: "api/payment-order-service"))

    private func configureJourney() {
        Resolver.register { self.pocketsUseCase as PocketsServiceUseCase }
        Resolver.register { self.arrangementsUseCase as ArrangementsServiceUseCase }
        Resolver.register { self.paymentsUseCase as PaymentsServiceUseCase }
        // Use this usecase to have payment success for testing purpose.
        // Resolver.register { AlwaysSucceedingPaymentsUseCase() as PaymentsServiceUseCase }
        Resolver.register { self.appConfiguration }
    }

    private func testDataProvider() -> TestDataProvider {
        var response: [TestBehavior]? = nil
        if let testBehavioursUrl = Bundle.main.url(forResource: "testbehaviours", withExtension: "json"),
            let testBehavioursData = try? Data(contentsOf: testBehavioursUrl),
            let testBehaviours = try? JSONDecoder().decode([TestBehavior].self, from: testBehavioursData) {
            response = testBehaviours
        }
        return TestDataProvider(testBehaviours: response)
    }

    // MARK: - Authentication Journey

    private var modalViewController: UIViewController?

    private lazy var authenticationUseCase: IdentityAuthenticationUseCase = { [weak self] in
        let usecase = IdentityAuthenticationUseCase(sessionChangeHandler: self?.handleSessionChange(newSession:))
        Backbase.register(authClient: usecase)

        Resolver.register { Authentication.Configuration() }
        Resolver.register { usecase as AuthenticationUseCase }

        return usecase
    }()

    @objc
    private func handleNavigationEvent(_ notification: Notification) {
        guard let navigation = authenticationUseCase.navigation(for: notification) else { return }
        DispatchQueue.main.async {
            switch navigation {
            case .dismissCurrent:
                self.modalViewController?.dismiss(animated: true)
            case .present(let screen):
                let navigationController = UINavigationController()
                let viewController = screen(navigationController)
                self.modalViewController = viewController
                self.window?.rootViewController?.present(viewController, animated: true)
            @unknown default:
                break
            }
        }
    }

    private func handleSessionChange(newSession session: Session) {
        DispatchQueue.main.async {
            let windowViewController: UIViewController
            let navigationController = UINavigationController()

            switch session {
            case .valid:
                DesignSystem.shared.retailStyles.navigationBar(navigationController.navigationBar)
                if #available(iOS 13.0, *) {
                    // keep empty for ios 13+
                } else {
                    navigationController.navigationBar.setBackgroundImage(nil, for: .default) // fix for ios 12
                }
                windowViewController = Overview.build(navigationController: navigationController)
            case .none:
                if self.authenticationUseCase.isEnrolled {
                    windowViewController = Login.build()(navigationController)
                } else {
                    windowViewController = Register.build()(navigationController)
                }
            case .locked:
                windowViewController = Register.build(session: .locked)(navigationController)
            case .expired:
                windowViewController = Login.build(session: .expired)(navigationController)
            @unknown default:
                fatalError()
            }
            navigationController.viewControllers = [windowViewController]
            self.window?.rootViewController = navigationController
        }
    }
}

private class AlwaysSucceedingPaymentsUseCase: PaymentsServiceUseCase {
    func postPaymentOrder(_ paymentOrder: RetailPocketsJourney.PaymentOrdersPost,
                          completion: @escaping (Result<RetailPocketsJourney.PaymentOrdersPostResponse,
                                                        ErrorResponse>) -> Void) {
        completion(.success(.init(identifier: "id", status: .accepted)))
    }
}
